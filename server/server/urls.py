from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.main_template),
    url(r'^login/?$', views.login_template),
    url(r'^profile/?$', views.profile_template),
    url(r'^profile/(?P<pk>[0-9]+)/?$', views.profile_user_template),
    url(r'^chat/?$', views.chat_template),
    url(r'^', include('server_api.urls')),
] + staticfiles_urlpatterns()
