from django.http import HttpResponseRedirect
from django.shortcuts import render


def login_template(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/')
    return render(request, 'login.html')


def main_template(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/login')
    return render(request, 'main.html')


def profile_template(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/login')
    return render(request, 'profile.html')


def profile_user_template(request, pk):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/login')
    return render(request, 'profile_user.html', {'pk': pk})


def chat_template(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/login')
    return render(request, 'chat.html')
