from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models import Q

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework import generics, permissions, pagination
from rest_framework.exceptions import ValidationError

from .models import Profile, Tag, Post, Reply, Like, LikeReply, Follow, Message, Prefer, Sim
from .serializers import TagSerializer, UserSerializer, PostSerializer, ReplySerializer, LikeSerializer, LikeReplySerializer, FollowSerializer, MessageSerializer

import os
import re
import secrets

from PIL import Image


class MemberPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.user
        if user.is_authenticated:
            return True
        else:
            return False


class OwnerPermission(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        user = request.user
        if not user.is_authenticated:
            return False
        elif obj.user == user:
            return True
        else:
            return False


admin_username_list = ['swpp201710']


class AdminPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.user
        if not user.is_authenticated:
            return False
        elif user.username in admin_username_list:
            return True
        return False


class PostPagination(pagination.CursorPagination):
    page_size = 10
    ordering = ('-created',)


@api_view(['POST'])
def login_view(request: Request):
    if request.user.is_authenticated:
        return Response(status=status.HTTP_200_OK)

    login_username = request.data.get('login_username', '')
    login_password = request.data.get('login_password', '')

    user = authenticate(username=login_username, password=login_password)

    if user is None:
        return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
    else:
        login(request, user)
        return Response(status=status.HTTP_200_OK)


@api_view(['POST'])
def logout_view(request: Request):
    logout(request)
    return Response(status=status.HTTP_200_OK)


pattern_username = re.compile(r'[a-zA-Z0-9_]{7,16}')
pattern_password = re.compile(r'.{9,32}')
pattern_email = re.compile(r'.+@.+\..+')
pattern_family_name = re.compile(r'[a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣]{1,16}')
pattern_given_name = re.compile(r'[a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣]{1,16}')

forbidden_username = ['admin', 'swpp']


@api_view(['POST'])
def join(request: Request):
    login_username = request.data.get('login_username', '')
    login_password = request.data.get('login_password', '')

    join_email = request.data.get('join_email', '')
    join_family_name = request.data.get('join_family_name', '')
    join_given_name = request.data.get('join_given_name', '')

    if re.fullmatch(pattern_username, login_username) is None:
        return Response('{"message": "아이디는 영어 대소문자, 숫자, 언더스코어(_)로 이뤄진 7-12 글자만 사용 가능해요"}',
                        content_type='application/json', status=status.HTTP_406_NOT_ACCEPTABLE)
    if re.fullmatch(pattern_password, login_password) is None:
        return Response('{"message": "비밀번호는 9-32 글자만 가능해요"}',
                        content_type='application/json', status=status.HTTP_406_NOT_ACCEPTABLE)
    if re.fullmatch(pattern_email, join_email) is None:
        return Response('{"message": "잘못된 이메일 형식이에요"}',
                        content_type='application/json', status=status.HTTP_406_NOT_ACCEPTABLE)
    if '+' in join_email:
        return Response('{"message": "\'+\' 문자는 메일 주소에 사용할 수 없어요"}',
                        content_type='application/json', status=status.HTTP_406_NOT_ACCEPTABLE)
    if re.fullmatch(pattern_family_name, join_family_name) is None:
        return Response('{"message": "성은 영어 대소문자, 한글로 최대 16 글자까지만 가능해요"}',
                        content_type='application/json', status=status.HTTP_406_NOT_ACCEPTABLE)
    if re.fullmatch(pattern_given_name, join_given_name) is None:
        return Response('{"message": "이름은 영어 대소문자, 한글로 최대 16 글자까지만 가능해요"}',
                        content_type='application/json', status=status.HTTP_406_NOT_ACCEPTABLE)

    for s in forbidden_username:
        if s in login_username:
            return Response('{"message": "\'%s\'는 아이디에 포함될 수 없어요"}' % s,
                            content_type='application/json', status=status.HTTP_406_NOT_ACCEPTABLE)

    if User.objects.filter(username=login_username).exists():
        return Response('{"message": "이미 사용 중인 아이디에요"}',
                        content_type='application/json', status=status.HTTP_406_NOT_ACCEPTABLE)
    if User.objects.filter(email=join_email).exists():
        return Response('{"message": "이미 사용 중인 이메일 주소에요"}',
                        content_type='application/json', status=status.HTTP_406_NOT_ACCEPTABLE)

    user = User.objects.create_user(login_username, join_email, login_password)
    user.first_name = join_family_name
    user.last_name = join_given_name
    user.save()

    return Response(status=status.HTTP_200_OK)


MAX_IMAGE_SIZE = 5 * 1024 * 1024


@api_view(['POST'])
def user_image_upload(request: Request):
    if 'img' not in request.FILES:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    user = request.user
    if not user.is_authenticated:
        return Response(status=status.HTTP_403_FORBIDDEN)

    file = request.FILES['img']
    if file.size > MAX_IMAGE_SIZE:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    original = Image.open(file)
    image_format = original.format
    if image_format != 'JPEG' and image_format != 'PNG':
        return Response(status=status.HTTP_400_BAD_REQUEST)

    w, h = original.size
    r = h / w

    try:
        profile = Profile.objects.get(user=user)
    except Profile.DoesNotExist:
        profile = Profile(user=user)

    while True:
        filename = secrets.token_urlsafe(12)
        try:
            profile.image = '/static/user/' + filename
        except ValidationError:
            continue
        break

    if w > h:
        width = 480
        height = int(480 * r)
    else:
        width = int(480 / r)
        height = 480

    image = original.resize((width, height), Image.ANTIALIAS)

    path = os.path.join(settings.MEDIA_ROOT, filename)
    image.save(path, 'JPEG', quality=80)

    profile.save()

    return Response({'path': '/static/user/' + filename}, status=status.HTTP_200_OK)


class TagListAPI(generics.ListAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    permission_classes = (MemberPermission,)


class TagCreateAPI(generics.CreateAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    permission_classes = (AdminPermission,)


class MemberProfileAPI(generics.ListAPIView):
    serializer_class = UserSerializer
    permission_classes = (MemberPermission,)

    def get_queryset(self):
        return User.objects.filter(username=self.request.user)


class UserUsernameAPI(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (MemberPermission,)


class UserPostAPI(generics.ListAPIView):
    serializer_class = PostSerializer
    pagination_class = PostPagination
    permission_classes = (MemberPermission,)

    def get_queryset(self):
        return Post.objects.filter(user=self.kwargs.get('pk', -1))


class UserFollowAPI(generics.ListAPIView):
    serializer_class = FollowSerializer
    permission_classes = (MemberPermission,)

    def get_queryset(self):
        return Follow.objects.filter(user=self.kwargs.get('pk', -1))


class UserSelfPostAPI(generics.ListAPIView):
    serializer_class = PostSerializer
    pagination_class = PostPagination
    permission_classes = (MemberPermission,)

    def get_queryset(self):
        return Post.objects.filter(user=self.request.user)


class PostCreateAPI(generics.ListCreateAPIView):
    serializer_class = PostSerializer
    pagination_class = PostPagination
    permission_classes = (MemberPermission,)

    def get_queryset(self):
        return Post.objects.filter(user__in=[o.follow for o in Follow.objects.filter(user=self.request.user)]
                                            + [self.request.user])

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

        tag = serializer.validated_data.get('tag', None)
        if tag is not None:
            prefer, _ = Prefer.objects.get_or_create(user=self.request.user, tag=tag)
            prefer.preference += 1.
            prefer.save()


class PostDeleteAPI(generics.DestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = (OwnerPermission,)


class ReplyPagination(pagination.CursorPagination):
    page_size = 10
    ordering = ('created',)


class ReplyCreateAPI(generics.ListCreateAPIView):
    serializer_class = ReplySerializer
    pagination_class = ReplyPagination
    permission_classes = (MemberPermission,)

    def get_queryset(self):
        post_pk = self.kwargs.get('post_pk', -1)
        return Reply.objects.filter(post=post_pk)

    def perform_create(self, serializer):
        post = Post.objects.get(pk=self.kwargs.get('post_pk', -1))
        serializer.save(user=self.request.user, post=post)


class ReplyDeleteAPI(generics.DestroyAPIView):
    serializer_class = ReplySerializer
    permission_classes = (OwnerPermission,)

    def get_queryset(self):
        pk = self.kwargs.get('pk', -1)
        post_pk = self.kwargs.get('post_pk', -1)
        return Reply.objects.filter(pk=pk, post=post_pk)


class LikeCreateAPI(generics.ListCreateAPIView):
    serializer_class = LikeSerializer
    permission_classes = (MemberPermission,)

    def get_queryset(self):
        post_pk = self.kwargs.get('post_pk', -1)
        return Like.objects.filter(post=post_pk)

    def perform_create(self, serializer):
        user = self.request.user

        try:
            post = Post.objects.get(pk=self.kwargs.get('post_pk', -1))
        except Post.DoesNotExist:
            raise ValidationError('Invalid post')

        if Like.objects.filter(user=user, post=post).exists():
            raise ValidationError('Unique constraint violation occurred')

        serializer.save(user=user, post=post)

        tag = post.tag
        if tag is not None:
            prefer, _ = Prefer.objects.get_or_create(user=self.request.user, tag=tag)
            prefer.preference += 1.
            prefer.save()


class LikeDeleteAPI(generics.DestroyAPIView):
    serializer_class = LikeSerializer
    permission_classes = (OwnerPermission,)

    def get_queryset(self):
        pk = self.kwargs.get('pk', -1)
        post_pk = self.kwargs.get('post_pk', -1)
        return Like.objects.filter(pk=pk, user=self.request.user, post=post_pk)


class LikeReplyCreateAPI(generics.ListCreateAPIView):
    serializer_class = LikeReplySerializer
    permission_classes = (MemberPermission,)

    def get_queryset(self):
        reply_pk = self.kwargs.get('reply_pk', -1)
        return LikeReply.objects.filter(reply=reply_pk)

    def perform_create(self, serializer):
        user = self.request.user

        try:
            reply = Reply.objects.get(pk=self.kwargs.get('reply_pk', -1))
        except Reply.DoesNotExist:
            raise ValidationError('Invalid reply')

        if LikeReply.objects.filter(user=user, reply=reply).exists():
            raise ValidationError('Unique constraint violation occurred')

        serializer.save(user=user, reply=reply)


class LikeReplyDeleteAPI(generics.DestroyAPIView):
    serializer_class = LikeReplySerializer
    permission_classes = (OwnerPermission,)

    def get_queryset(self):
        pk = self.kwargs.get('pk', -1)
        reply_pk = self.kwargs.get('reply_pk', -1)
        return LikeReply.objects.filter(pk=pk, user=self.request.user, reply=reply_pk)


class FollowRecommendAPI(generics.ListAPIView):
    serializer_class = UserSerializer
    permission_classes = (MemberPermission,)

    def get_queryset(self):
        user = self.request.user
        sim_best = Sim.objects.filter(Q(user1=user) | Q(user2=user)).exclude(
            user1__in=[o.follow for o in Follow.objects.filter(user=self.request.user)]
        ).exclude(
            user2__in=[o.follow for o in Follow.objects.filter(user=self.request.user)]
        )[:4]

        if len(sim_best) == 4:
            user_list = []
            for sim in sim_best:
                if sim.user1 == user:
                    user_list.append(sim.user2.id)
                else:
                    user_list.append(sim.user1.id)

            return User.objects.filter(pk__in=user_list)
        else:
            return User.objects.exclude(
                pk__in=[o.follow.pk for o in Follow.objects.filter(user=self.request.user)]
            ).order_by('?')[:4]


class FollowCreateAPI(generics.ListCreateAPIView):
    serializer_class = FollowSerializer
    permission_classes = (MemberPermission,)

    def get_queryset(self):
        return Follow.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        user = self.request.user

        if user == serializer.validated_data['follow']:
            raise ValidationError('Cannot follow yourself')
        if Follow.objects.filter(user=user, follow=serializer.validated_data['follow']).exists():
            raise ValidationError('Unique constraint violation occurred')

        serializer.save(user=user)


class FollowDeleteAPI(generics.DestroyAPIView):
    queryset = Follow.objects.all()
    serializer_class = FollowSerializer
    permission_classes = (OwnerPermission,)


class MessageCreateAPI(generics.CreateAPIView):
    serializer_class = MessageSerializer
    permission_classes = (MemberPermission,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class MessageFromListAPI(generics.ListAPIView):
    serializer_class = MessageSerializer
    permission_classes = (MemberPermission,)

    def get_queryset(self):
        return Message.objects.filter(user=self.request.user)


class MessageToListAPI(generics.ListAPIView):
    serializer_class = MessageSerializer
    permission_classes = (MemberPermission,)

    def get_queryset(self):
        return Message.objects.filter(to=self.request.user)
