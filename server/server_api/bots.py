from background_task import background

import numpy as np

import requests
from bs4 import BeautifulSoup

import re

from datetime import datetime
import pytz


def refresh():
    from .models import Bot
    print('refresh bots...')
    for bot in Bot.objects.all():
        if bot.running:
            continue

        if bot.type == 'naver':
            bot_naver(bot.user.username, bot.url, bot.tag.name, repeat=bot.period)
        elif bot.type == 'prefer':
            bot_prefer(bot.user.username, repeat=bot.period)

        bot.running = True
        bot.save()

        print(bot.user.username, 'bot is now running')


DATE_TEXT = re.compile(r'(\d{4})\.(\d{2})\.(\d{2})')
TZ = pytz.timezone('Asia/Seoul')


def abs_url(top, url):
    if url[0] == '/':
        return top + url
    else:
        return url


@background()
def bot_naver(username, url, tag_name):
    from django.contrib.auth.models import User
    from .models import Post, Bot, Tag, Prefer

    from django.utils import timezone

    print('naver bot is running:', username)

    bot = Bot.objects.get(user__username=username)
    user = User.objects.get(username=username)
    tag = Tag.objects.get(name=tag_name)

    last_update = bot.last_update

    doc_front = requests.get(url).text

    soup_front = BeautifulSoup(doc_front, 'html.parser')
    main_frame = soup_front.select_one('#mainFrame')['src']
    main_frame = abs_url(url, main_frame)

    doc_main = requests.get(main_frame).text

    soup_main = BeautifulSoup(doc_main, 'html.parser')

    for post in soup_main.select('dd'):
        if not post.select('img'):
            continue

        try:
            img = post.select_one('img')
            src = abs_url(url, img['src'])
            title = ''
            for a in post.select('a'):
                if a.text:
                    title = a.text.strip()
                    break
            href = abs_url(url, post.select_one('a')['href'])
            date_raw = DATE_TEXT.search(str(post)).groups()
            date = datetime(int(date_raw[0]), int(date_raw[1]), int(date_raw[2]), tzinfo=TZ)

            if last_update < date:
                Post.objects.create(user=user, tag=tag, title=title,
                                    body='<img src="%s">\n<a href="%s" target="_blank">%s</a>'
                                         % (src, href, title))
        except:
            continue

    if tag is not None:
        prefer, _ = Prefer.objects.get_or_create(user=user, tag=tag)
        prefer.preference += 1.
        prefer.save()

    bot.last_update = timezone.now()
    bot.save()

    print('job done:', username)


@background()
def bot_prefer(username):
    from django.contrib.auth.models import User
    from .models import Tag, Prefer, Sim

    print('prefer bot is running:', username)

    tag_list = [Tag.objects.get(pk=pk) for pk in range(1, 11)]

    for user1 in User.objects.all():
        pk1 = user1.id

        prefer1 = []
        for tag in tag_list:
            prefer1.append(Prefer.objects.get_or_create(user=user1, tag=tag)[0].preference)

        prefer1 = np.array(prefer1)
        prefer1_norm = np.sqrt(np.sum(np.square(prefer1)))
        if prefer1_norm == 0.:
            continue
        prefer1 /= prefer1_norm

        for user2 in User.objects.all():
            pk2 = user2.id
            if pk1 >= pk2:
                continue

            prefer2 = []
            for tag in tag_list:
                prefer2.append(Prefer.objects.get_or_create(user=user2, tag=tag)[0].preference)

            prefer2 = np.array(prefer2)
            prefer2_norm = np.sqrt(np.sum(np.square(prefer2)))
            if prefer2_norm == 0.:
                continue
            prefer2 /= prefer2_norm

            cos_sim = np.sum(np.dot(prefer1, prefer2))

            sim, _ = Sim.objects.get_or_create(user1=user1, user2=user2)
            sim.sim = cos_sim
            sim.save()

    print('job done:', username)
