from django.contrib import admin
from .models import Post, Reply, Like, Bot, Profile, Tag, Prefer, Sim

admin.site.register(Post)
admin.site.register(Reply)
admin.site.register(Like)
admin.site.register(Bot)
admin.site.register(Profile)
admin.site.register(Tag)
admin.site.register(Prefer)
admin.site.register(Sim)
