from django.db import models


class Profile(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, primary_key=True)
    image = models.CharField(max_length=16, unique=True, blank=True, default=None, null=True)


class Tag(models.Model):
    name = models.TextField()

    def __str__(self):
        return str(self.id) + ': ' + self.name


class Post(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    tag = models.ForeignKey(Tag, on_delete=models.SET_NULL, default=None, blank=True, null=True)
    title = models.TextField(default='', blank=True)
    body = models.TextField()

    class Meta:
        ordering = ('-created',)


class Reply(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    body = models.TextField()

    class Meta:
        ordering = ('created',)


class Like(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('created',)
        unique_together = (('user', 'post'),)


class LikeReply(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    reply = models.ForeignKey(Reply, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('created',)
        unique_together = (('user', 'reply'),)


class Follow(models.Model):
    user = models.ForeignKey('auth.User', related_name='follow_user', on_delete=models.CASCADE)
    follow = models.ForeignKey('auth.User', related_name='follow_follow', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-created',)
        unique_together = (('user', 'follow'),)


class Bot(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, primary_key=True)
    type = models.TextField()
    url = models.TextField()
    tag = models.ForeignKey(Tag, on_delete=models.SET_NULL, default=None, blank=True, null=True)
    period = models.IntegerField(blank=True, default=3600)
    last_update = models.DateTimeField(blank=True, default=None, null=True)
    running = models.BooleanField(blank=True, default=False)

    def __str__(self):
        return self.user.username


class Message(models.Model):
    user = models.ForeignKey('auth.User', related_name='message_user', on_delete=models.CASCADE)
    to = models.ForeignKey('auth.User', related_name='message_to', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    body = models.TextField()

    class Meta:
        ordering = ('-created',)


class Prefer(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
    preference = models.FloatField(blank=True, default=0.)

    class Meta:
        unique_together = (('user', 'tag'),)


class Sim(models.Model):
    user1 = models.ForeignKey('auth.User', related_name='sim_user1', on_delete=models.CASCADE)
    user2 = models.ForeignKey('auth.User', related_name='sim_user2', on_delete=models.CASCADE)
    sim = models.FloatField(blank=True, default=0.)

    class Meta:
        ordering = ('-sim',)
        unique_together = (('user1', 'user2'),)
