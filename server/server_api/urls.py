from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^api/login/?$', views.login_view),
    url(r'^api/logout/?$', views.logout_view),

    url(r'^api/join/?$', views.join),

    url(r'^api/user/?$', views.MemberProfileAPI.as_view()),
    url(r'^api/user/(?P<pk>[0-9]+)/?$', views.UserUsernameAPI.as_view()),
    url(r'^api/user/(?P<pk>[0-9]+)/follow/?$', views.UserFollowAPI.as_view()),
    url(r'^api/user/post/?$', views.UserSelfPostAPI.as_view()),
    url(r'^api/user/(?P<pk>[0-9]+)/post/?$', views.UserPostAPI.as_view()),
    url(r'^api/user/image/upload/?$', views.user_image_upload),

    url(r'^api/follow/?$', views.FollowCreateAPI.as_view()),
    url(r'^api/follow/(?P<pk>[0-9]+)/?$', views.FollowDeleteAPI.as_view()),
    url(r'^api/follow/rec/?$', views.FollowRecommendAPI.as_view()),

    url(r'^api/tag/?$', views.TagListAPI.as_view()),
    url(r'^api/tag/add/?$', views.TagCreateAPI.as_view()),

    url(r'^api/post/?$', views.PostCreateAPI.as_view()),
    url(r'^api/post/(?P<pk>[0-9]+)/?$', views.PostDeleteAPI.as_view()),

    url(r'^api/post/(?P<post_pk>[0-9]+)/reply/?$', views.ReplyCreateAPI.as_view()),
    url(r'^api/post/(?P<post_pk>[0-9]+)/reply/(?P<pk>[0-9]+)/?$', views.ReplyDeleteAPI.as_view()),

    url(r'^api/post/(?P<post_pk>[0-9]+)/like/?$', views.LikeCreateAPI.as_view()),
    url(r'^api/post/(?P<post_pk>[0-9]+)/like/(?P<pk>[0-9]+)/?$', views.LikeDeleteAPI.as_view()),

    url(r'^api/reply/(?P<reply_pk>[0-9]+)/like/?$', views.LikeReplyCreateAPI.as_view()),
    url(r'^api/reply/(?P<reply_pk>[0-9]+)/like/(?P<pk>[0-9]+)/?$', views.LikeReplyDeleteAPI.as_view()),

    url(r'^api/message/?$', views.MessageCreateAPI.as_view()),
    url(r'^api/message/from/?$', views.MessageFromListAPI.as_view()),
    url(r'^api/message/to/?$', views.MessageToListAPI.as_view()),
]
