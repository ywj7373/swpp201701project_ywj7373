from rest_framework import serializers

from django.contrib.auth.models import User

from .models import Profile, Tag, Post, Reply, Like, LikeReply, Follow, Message, Prefer, Sim


class UserSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'username', 'image')

    def get_image(self, obj):
        try:
            return Profile.objects.get(user=obj).image
        except Profile.DoesNotExist:
            return ''


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name')


class PostSerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = ('id', 'user', 'username', 'created', 'tag', 'title', 'body')
        read_only_fields = ('user', 'username', 'created',)

    def get_username(self, obj):
        return obj.user.username


class ReplySerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField()

    class Meta:
        model = Reply
        fields = ('id', 'user', 'username', 'post', 'created', 'body')
        read_only_fields = ('user', 'username', 'post', 'created',)

    def get_username(self, obj):
        return obj.user.username


class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = ('id', 'user', 'post', 'created')
        read_only_fields = ('user', 'post', 'created',)


class LikeReplySerializer(serializers.ModelSerializer):
    class Meta:
        model = LikeReply
        fields = ('id', 'user', 'reply', 'created')
        read_only_fields = ('user', 'reply', 'created',)


class FollowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Follow
        fields = ('id', 'user', 'follow', 'created')
        read_only_fields = ('user', 'created',)


class MessageSerializer(serializers.ModelSerializer):
    user_username = serializers.SerializerMethodField()
    to_username = serializers.SerializerMethodField()

    class Meta:
        model = Message
        fields = ('id', 'user', 'user_username', 'to', 'to_username', 'created', 'body')
        read_only_fields = ('user', 'user_username', 'to_username', 'created',)

    def get_user_username(self, obj):
        return obj.user.username

    def get_to_username(self, obj):
        return obj.to.username
