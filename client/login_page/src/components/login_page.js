import React from 'react'
import { Button, Input, Form, Title, Box, Box2, Status, Message, Link } from './css'

export const LoginPane = ({ toggle, loginFail, joinFail, loginRequest, joinRequest, joinOrLogin, isLogin }) => {
    let name, pwd, email, familyName, givenName;
    
    const onLogin = (e) => {
        e.preventDefault()
        if (name !== undefined && pwd !== undefined) {
            loginRequest(name.value, pwd.value);
         }
    }

    const onJoin = (e) => {
        e.preventDefault()
        if (name !== undefined && pwd !== undefined && email !== undefined &&
        familyName !== undefined && givenName !== undefined) {
            joinRequest(name.value, pwd.value, email.value, familyName.value, givenName.value)
        }
    }

    const join = () => {
        return (
            <div>
                <Box2 className="join_page">
                    <Title>Folivora</Title>
                    <Form className="register_form" onSubmit={onJoin}>
                        <Input type="text" placeholder="Username" 
                            id="join_username" innerRef={n => {name = n;}}/>
                        <Input type="password" placeholder="Password"
                            id="join_password" innerRef={n => {pwd = n;}}/>
                        <Input type="text" placeholder="Email Address"
                            id="join_email" innerRef={n => {email = n;}}/>
                        <Input type="text" placeholder="Family Name"
                            id="family_name_field" innerRef={n => {familyName = n;}}/>
                        <Input type="text" placeholder="Given Name"
                            id="given_name_field" innerRef={n => {givenName = n;}}/>
                        <Status id="error_message">{joinFail}</Status>
                        <Button type='submit' 
                            id="join_button" className="join_button">Join</Button>
                        <Message>Already registered?
                            <Link onClick={joinOrLogin}
                           	    id="change_login_button"> Sign In
			                </Link>
		                </Message>
                    </Form>
                </Box2>
            </div>
        )
    }

    const login = () => {
        return (
            <div>
                <Box className="login_page">
                    <Title>Folivora</Title>
                    <Form className="login_form" onSubmit={onLogin}>
                        <Input type="text" placeholder="Username" 
                            id="login_username" innerRef={n => {name = n;}} />
                        <Input type="password" placeholder="Password" 
                            id="login_password" innerRef={n => {pwd = n;}} />
                        <p id="error_message_2" 
                            style={loginFail?{"color": "red", "fontSize": "14px",
                            "margin": "0 0 10px 0", "fontFamily":"Tahoma"}:
			                {"display": "none"}}> Wrong username or password</p>
                        <Button type='submit' id="login_button">Login</Button>
                        <Message>Not registered?  
                            <Link onClick={joinOrLogin}
                                id="change_join_button"> Join
			                </Link>
			            </Message>
                    </Form>
                </Box>
            </div>
        )
    }
    
    if (!toggle) {
        return login()
    }
    else {
        return join()
    }
}

export default LoginPane
