import styled from 'styled-components'

export const Button = styled.button`
    outline: 0;
    background: #58D68D;
    width: 100%;
    border: 0;
    padding: 15px;
    color: black;
    fontSize: 16px;
    cursor: pointer;
    font-family: Verdana;
    border-radius: 25px;
    &:hover {
        background: #33FF63;
    }
`;

export const Input = styled.input`
    border-radius: 25px;
    outline: 0;
    background: #f2f2f2;
    width: 100%;
    border: 0;
    margin: 0 0 15px;
    padding: 15px;
    boxSizing: border-box;
    font-size: 14px;
`;
    
export const Form = styled.form`
    zIndex: 1;
    position: relative;
    maxWidth: 360px;
    margin: auto;
    padding: 45px;
    text-align: center;
`;

export const Title = styled.div`
    margin-top: 20px;
    margin-bottom: 20px;
    font-family: fantasy;
    color: #58D68D;
    text-align: center;
    font-size: 40px;
`;

export const Box = styled.div`
    width: 400px;
    height: 400px;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
    padding: 10px 0px 10px 0px;
    background: rgba(0, 0, 0, 0.8);
    border-radius: 10px;
`;

export const Box2 = styled.div`
    width: 400px;
    height: 600px;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
    padding: 10px 0px 10px 0px;
    background: rgba(0, 0, 0, 0.8);
    border-radius: 10px;
`;

export const Status = styled.p`
    margin: 0 0 10px 0;
    color: red;
    font-size: 14px;
    font-family: Tahoma;
`; 

export const Message = styled.p`
    margin: 15px 0 0;
    color: #b3b3b3;
    font-size: 14px;
`;

export const Link = styled.a`
    color: #58D68D;
    text-decoration: none;
    cursor: pointer;
`;
