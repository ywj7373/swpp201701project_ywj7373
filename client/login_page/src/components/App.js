import React from 'react'
import LoginPane from '../containers/login_container'

const App = () => {
    return (
        <div>
            <LoginPane />
        </div>
    );
}

export default App
