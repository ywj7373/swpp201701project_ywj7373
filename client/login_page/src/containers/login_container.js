import { connect } from 'react-redux'
import { LoginPane } from '../components/login_page'
import { joinRequest, loginRequest, joinOrLogin} from '../actions/login_action'

const mapStateToProps = (state) => {
    return {
        toggle: state.toggle,
        loginFail: state.loginFail,
        joinFail: state.joinFail
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        joinRequest : (uname, upwd, email, fname, gname) => {
            dispatch(joinRequest(uname, upwd, email, fname, gname))
        },
        loginRequest : (uname, upwd) => {
            dispatch(loginRequest(uname, upwd))
        },
        joinOrLogin : () => {
            dispatch(joinOrLogin())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPane)
