import $ from 'jquery'

const login_url = "/api/login/"
const join_url = "/api/join/"

export const joinOrLogin = () => {
    return {
        type: 'JOIN_OR_LOGIN'
    }
}

export const loginFailure = () => {
    return {
        type: 'LOGIN_FAILURE'
    }
}

export const joinFailure = (joinFail) => {
    return {
        type: 'JOIN_FAILURE',
        joinFail
    }
}

export function joinRequest (uname, upwd, email, fname, gname) {
    let csrftoken = document.getElementById("csrftoken").value
    const request = $.post(join_url, {
            login_username: uname,
            login_password: upwd,
            join_email: email,
            join_family_name: fname,
            join_given_name: gname,
            csrfmiddlewaretoken: csrftoken
            })
    
    return (dispatch) => {
        request.then(response => {
            dispatch(joinOrLogin())
        }).catch (error => {
            if(error.status === 406) {
                dispatch(joinFailure(JSON.parse(error.responseJSON).message))
            }
        })
    }  
}

export function loginRequest (uname, upwd) {
    let csrftoken = document.getElementById("csrftoken").value   
    
    const request = $.post(login_url, {
        login_username: uname,
        login_password: upwd,
        csrfmiddlewaretoken: csrftoken
    })
    
    return (dispatch) => {
        request.then(response => {
            window.location.replace("/")
        }).catch (error => {
            dispatch(loginFailure())
        })
        
    }
}
