import $ from 'jquery'

const logout_url = "/api/logout/"
const user_url = "/api/user/"
const get_user_url = (id) => {
    return user_url + id + "/"
}
const post_url = "/api/post/"
const reply_url = (id) => {
    return post_url + id + "/reply/"
}
const like_url = (id) => {
    return post_url + id + "/like/"
}
const follow_url = "/api/follow/"
const follower_url = "/api/follow/rec/"
const tag_url = "/api/tag/"
const add_tag_url = "/api/tag/add/"
const get_post_url = (id) => {
    return post_url + id + "/"
}
const get_reply_url = (pid, rid) => {
    return post_url + pid + "/reply/" + rid + "/"
}
const get_like_url = (pid, lid) => {
    return post_url + pid + "/like/" + lid + "/"
}
const reply_like_url = (rid) => {
    return "/api/reply/" + rid + "/like/"
}
const get_reply_like_url = (rid, lid) => {
    return reply_like_url(rid) + lid
}

var token = document.getElementById("csrftoken").value

//Initial actions: checkEmptyPostList, getLastPostId, getFollowingList, getInitialPost, addPost, addReply, addLike, deleteLike, deleteReply, loadReply, logout
//getFollowingList -> pushFollowing
//checkEmptyPostList -> setIsEmpty
//getLastPostId -> setLastPostId
//getInitialPost -> pushPosts, loadReply, loadLike
//addPost -> pushPost -> updatePostList
//addReply -> pushReply -> updateReplyList
//addLike -> loadReply -> updateLikeList
//deleteLike-> loadReply -> updateLikeList
//deleteReply -> getNextReplies -> deleteReplyFromList
//loadLike -> updateLikeList
//loadReply -> pushReplies

//initial actions updating state
export function updateState() {
    return dispatch => {
       dispatch(getUserInfo())
       dispatch(checkEmptyPostList())
       dispatch(getLastPostId(post_url))
       dispatch(getFollowingList())
       dispatch(getFollowerList())
       dispatch(getTagList())
    }
}

export function getUserInfo() {
    const request = $.get(user_url)

    return dispatch => {
        request.done(function(data) {
            if (data.length === 1) {
                let user = data[0]
                dispatch(setUserInfo(user.id, user.username, user.image))
            }
         })
    }
}

export const setUserInfo = (id, username, image) => {
    return {
        type: 'SET_USER_INFO',
        id,
        username,
        image
    }
}

export function checkEmptyPostList() {
    const request = $.get(post_url)

    return dispatch => {
        request.done(function(data) {
            dispatch(setIsEmpty(data.results, data.next))
        })
    }
}

export const setIsEmpty = (posts, next) => {
    return {
        type: 'SET_IS_EMPTY',
        posts,
        next
    }
}

export function getLastPostId(url) {
    const request = $.get(url)

    return dispatch => {
        request.done(function(data) {
            if (data.next !== null) {
                dispatch(getLastPostId(data.next))
            }
            else {
                dispatch(setLastPostId(data.results))
            }
        })
    }
}

export const setLastPostId = (posts) => {
    return {
        type: 'SET_LAST_POST_ID',
        posts
    }
}

export function getFollowingList() {
    const request = $.get(follow_url)

    return dispatch => {
        request.done(function(data) {
            let len = data.length
            let i = 0
            while (i < len) {
                dispatch(getFollowUsername(data[i].follow))
                i++
            }
        })
    }
}

export function getFollowUsername(id) {
    const request = $.get(get_user_url(id))

    return dispatch => {
        request.done(function(data) {
            dispatch(pushFollowing(id, data.username, data.image))
        })
    }
}

export const pushFollowing = (user, username, image) => {
    return {
        type: 'PUSH_FOLLOWING',
        user,
        username,
        image
    }
}

export function getFollowerList() {
    const request = $.get(follower_url)

    return dispatch => {
        request.done(function(data) {
            dispatch(pushFollower(data))
        })
    }
}

export const pushFollower = (recFollows) => {
    return {
        type: 'PUSH_FOLLOWER',
        recFollows
    }
}

export function getTagList() {
    const request = $.get(tag_url)

    return dispatch => {
        request.done(function(data) {
            dispatch(setTagList(data))
        })
    }
}

export const setTagList = (tags) => {
    return {
        type: 'SET_TAG_LIST',
        tags
    }
}

//initial post, reply, like actions
export function getInitialPost(url) {
    const request = $.get(url)

    return dispatch => { 
	    request.done(function(data) {
            let len = data.results.length
            let i = 0
            let j = 0
            let k = 0

            while (i < len) { 
                let post = data.results[i]
                post.body = post.body.replace(/\n/g, '<br>').replace(/<img[^>]+>/g, '');
                dispatch(pushPosts(post.id, post.user, post.username, 
                    post.tag, post.title, post.body, post.created))
                i++
            }

            while (j < len) {
                let post = data.results[j]
                dispatch(loadReply(post.id, reply_url(post.id)))
                j++
            }

            while (k < len) {
                dispatch(loadLike(data.results[k].id))
                k++
            }
            
            dispatch(updateNextUrl(data.next))
	    })
    }
}

export const updateNextUrl = (url) => {
    return {
        type: 'UPDATE_NEXT_URL',
        url
    }
}

//post actions
export const pushPosts = (id, user, username, tag, title, body, created) => {
    return {
        type: 'PUSH_POSTS',
        id,
        user,
        username,
        tag,
        title,
        body,
        created,
        replies: [],
        likes: [],
        nextReplies: null,
        nextRepliesNum: 0
    }
}

export function addPost(text, titleText, tag) {
    const request = $.post(post_url, {
        tag: tag,
        title: titleText,
        body: text,
        csrfmiddlewaretoken: token
    })

    return dispatch => {
        request.done(response => {
            dispatch(pushPost())
        })
    }
}

export function pushPost() {
    const request = $.get(post_url)

    return dispatch => {
        request.done(function(data) {
            let post = data.results[0];
            post.body = post.body.replace(/\n/g, '<br>').replace(/<img[^>]+>/g, '');
            dispatch(updatePostList(post.id, post.user, post.username, post.tag,
                post.title, post.body, post.created))
            dispatch(updateNextUrl(data.next))
        })
    }
}

export const updatePostList = (id, user, username, tag, title, body, created) => {
    return {
        type: 'UPDATE_POST_LIST',
        id,
        user,
        username,
        tag,
        title,
        body,
        created,
        replies: [],
        likes: [],
        nextReplies: null,
        nextRepliesNum: 0
    }
}

//reply actions
export function loadReply(id, url) {
    const request = $.get(url)

    return dispatch => {
        request.done(function(data) {
            let len = data.results.length
            let i = 0
            while (i < len) { 
                let reply = data.results[i]
                dispatch(getReplyLike(id, reply.id, reply.user, 
                    reply.username, reply.created, reply.post, 
                    reply.body, data.next))
                i++
            }
        })
    }
}

export function getReplyLike(pid, rid, user, username, created, post, body, nextReplies ) {
    const request = $.get(reply_like_url(rid))

    return dispatch => {
        request.done(function(data) {
            dispatch(pushReplies(pid, rid, user, username, 
                created, post, body, data, nextReplies))
        })
    }
}
    

export const pushReplies = (pid, rid, user, username, created, post, body, like, nextReplies) => {
    return {
        type: 'PUSH_REPLIES',
        pid,
        rid,
        user,
        username,
        created,
        post,
        body,
        like,
        nextReplies
    }
}

export function addReply(id, text) {
    const request = $.post(reply_url(id), {
        body: text,
        csrfmiddlewaretoken: token
    })

    return dispatch => {
        request.done(response => {
            dispatch(pushReply(id, reply_url(id)))
        })
    }
}

export function pushReply(id, url) {
    const request = $.get(url)

    return dispatch => {
        request.done(function(data) {
            if (data.next !== null) {
                dispatch(pushReply(id, data.next))
            }
            else {
                if (data.previous !== null) {
                    $.get(data.previous).done(function(x) {
                        let reply = data.results[data.results.length-1]
                        dispatch(updateReplyList(id, reply.id, reply.user, 
                            reply.username, reply.created, reply.post, 
                            reply.body, x.next))
                    })
                }
                else {
                    let reply = data.results[data.results.length-1]
                    dispatch(updateReplyList(id, reply.id, reply.user, 
                        reply.username, reply.created, 
                        reply.post, reply.body, null))
                }
            }
        })
    }
}

export const updateReplyList = (id, rid, user, username, created, post, body, nextReplies) => {
    return {
        type: 'UPDATE_REPLY_LIST',
        id,
        rid,
        user,
        username,
        created,
        post,
        body,
        like: [],
        nextReplies
    }
}

export function deleteReply(pid, id, num) {
    return dispatch => {
	    function csrfSafeMethod(method) {
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", token);
                }
            }
        })        
        
        $.ajax({
            url: get_reply_url(pid, id),
            type: 'DELETE',
            success: function(result) {
                dispatch(getNextReplies(reply_url(pid), pid, id, num))
            }
        })
    }
}

export function getNextReplies(url, pid, id, num) {
    const request = $.get(url)
 
    return dispatch  => {
        request.done(function(data) {
            if (num > 1 && data.next !== null) {
                let n = num - 1
                dispatch(getNextReplies(data.next, pid, id, n))
            }
            else if (data.next !== null) {
                dispatch(deleteReplyFromList(pid, id, data.results[9], data.next))
            }
            else {
                if (data.results.length < 10) {
                    dispatch(deleteReplyFromList(pid, id, null, null))
                }
                else {
                    dispatch(deleteReplyFromList(pid, id, data.results[9], null))
                }
            }
        })
    }
}

export const deleteReplyFromList = (pid, id, newReply, nextReplies) => {
    return {
        type: 'DELETE_REPLY_FROM_LIST',
        pid,
        id,
        newReply,
        nextReplies
    }
}

export function addReplyLike(pid, rid) {
    const request = $.post(reply_like_url(rid), {
        csrfmiddlewaretoken: token
    })

    return dispatch => {
        request.done(function(data) {
            dispatch(getLikeObject(pid, rid))
        })
    }
}

export function getLikeObject(pid, rid) {
    const request = $.get(reply_like_url(rid))

    return dispatch => {
        request.done(function(data) {
            dispatch(updateReplyLikeList(pid, rid, data))
        })
    }
}

export const updateReplyLikeList = (pid, rid, like) => {
    return {
        type: 'UPDATE_REPLY_LIKE_LIST',
        pid,
        rid,
        like
    }
}

export function deleteReplyLike(pid, rid, lid) { 
    return dispatch => {
	    function csrfSafeMethod(method) {
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", token);
                }
            }
        })        
        
        $.ajax({
            url: get_reply_like_url(rid, lid),
            type: 'DELETE',
            success: function(result) {
                dispatch(getLikeObject(pid, rid))
            }
        })
    }
}

//like actions
export function loadLike(id) {
    const request = $.get(like_url(id))

    return dispatch => {
        request.done(function(data) {
            dispatch(updateLikeList(id, data))
        })
    }
}

export const updateLikeList = (id, likes) => {
    return {
        type: 'UPDATE_LIKE_LIST',
        id,
        likes
    }
}

export function addLike(id) {
    const request = $.post(like_url(id), {
        csrfmiddlewaretoken: token
    })

    return dispatch => {
        request.done(response => {
            dispatch(loadLike(id))
        })
    }
}

export function deleteLike(id, likeId) {
    return dispatch => {
        function csrfSafeMethod(method) {
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }
        
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", token);
                }
            }
        });

        $.ajax({
            url: get_like_url(id, likeId),
            type: 'DELETE',
            success: function(result) {
                dispatch(loadLike(id))
            }
        })
   }
}  

//follow actions
export function addFollow(fid) {
    const request = $.post(follow_url, {
        follow: fid,
        csrfmiddlewaretoken: token
    })

    return dispatch => {
        request.done(response => {
            dispatch(getFollowUsername(fid))
            dispatch(getFollowerList())
        })
    }
}

//page actions
export const toggleLoad = (toggle) => {
    return {
        type: 'TOGGLE_LOAD',
        toggle
    }
}


export function logout() {
    const request = $.post(logout_url, {
        csrfmiddlewaretoken: token
    })

    return dispatch => {
        request.done(response => {
            window.location.replace("/login/")
        })
    }
}

export function newProfile(id) {
    return dispatch => {
        window.location.replace("/profile/" + id)
    }
}
