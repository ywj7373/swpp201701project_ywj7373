import styled from 'styled-components'

//App
export const Screen = styled.div`
    font-family: PT sans
`;

export const Main = styled.div`
    padding: 90px 0 0 0;
    margin: 0 auto;
    max-width: 1200px;
`;

export const MainContainer = styled.div`
    display: inline-block;
    width: 60%;
    margin: 0 6.5% 0 0;
`;

//navbar
export const UL = styled.ul`
    list-style-type: none;
    margin: 0;
    padding 20px 50px 20px 0;
    overflow: hidden;
    background-color: white;
    text-align: right;
    height: 50px;
    position: fixed;
    top: 0;
    right: 0;
    left: 0;
    width: 100%;
    border-bottom: 1px solid #CCD1D1;
    z-index: 2;
    box-shadow: 0 2px 3px #CCD1D1
`;

export const A = styled.a`
    color: #555555;
    font-size: 14px;
    font-weight: bold;
    text-decoration: none;
    cursor: pointer;
    &:hover {
       color: #58D68D;
    }
`;

export const Vertical = styled.div`
    color: #555555;
    font-size: 14px;
`;

export const A2 = styled.a`
    text-decoration: none;
    color: #58D68D;
    cursor: pointer;
`;

export const LI = styled.li`
    display: inline-block;
    padding: 13px 15px 0 0;
`;

export const LI2 = styled.li`
    display: inline-block;
    padding: 13px 24% 0 0;
`;

export const Logo = styled.li`
    float: left;
    font-family: lobster two;
    font-size: 40px;
    color: #58D68D;
    padding: 0 0 0 17.5%;
`;

//post form
export const User = styled.div`
    height: 150px;
`;

export const Info = styled.div`
    width: 50%;
    height: 80px;
    margin: 5% 0 0 3%;
    float: left;
`;

export const Box = styled.div`
    width: 556px;
    background-color: white;
    border: 1px solid #DDDDDD;
    border-radius: 5px;
    padding: 20px 20px 0 20px;
    margin: 0 10% 2% 20%;
`;

export const FormBox = styled.div`
    width: 600px;
    margin: 0 5% 4% 20%;
`;

export const Image = styled.img`
    border-radius: 50%;
    margin: 3% 0 0 ;
    width: 100px;
    height: 100px;
    background-position: center center;
    background-size: cover;
    float: left;
`;

export const Username = styled.span`
    font-size: 35px;
    z-index: 1;
`;

export const UsernameA = styled.a`
    color: #697689;
    font-weight: bold;
    text-decoration: none;
`;

export const Follow = styled.div`
    font-size: 15px;
    color: #697689;
    margin-left: 3px;
    margin-top: 10px;
`;

export const Hr = styled.div`
    background: #EEEEEE;
    height: 1px;
    margin: 0 0 1% 0;
`;

export const Title = styled.input`
    height: 25px;
    width: 591px;
    resize: none;
    border: 1px solid #DDDDDD;
    border-radius: 5px;
    font-family: PT sans;
    padding: 2px
    font-size: 14px;
    color: #697689;
    &::placeholder {
        font-family: pt Sans;
        font-size: 14px;
    }
`;

export const Textarea = styled.textarea`
    margin: 1% 0 0 0;
    height: 80px;
    width: 591px;
    resize: none;
    border: 1px solid #DDDDDD;
    border-radius: 5px;
    font-family: PT sans;
    font-size: 14px;
    color: #697689;
    &::placeholder {
        font-family: PT sans;
        font-size: 14px
    }
`;

export const Select = styled.select`
    display: inline-block;
    border: 1px solid #DDDDDD;
    border-radius: 5px;
    font-size: 14px;
    color: #697689;
`;

export const Button = styled.button`
    display: inline-block;
    background: #58D68D;
    border: 0;
    padding: 8px;
    color: white;
    fontSize: 14px;
    cursor: pointer;
    border-radius: 5px;
    height: 30px;
    margin: 0 0 0 410px;
    &:hover {
        background: #33FF63;
    }
`;

//post list
export const Head = styled.div`
    width: 540px;
    font-size: 18px;
    margin: 0 0 1% 10px;
    color: #444444;
    word-wrap: break-word;
`;

export const Body = styled.div`
    width: 540;
    font-size: 14px;
    margin: 0 0 1% 10px;
    color: #555555;
    word-wrap: break-word;
`;

export const Like = styled.div`
    margin: 0 0 2% 2%;
    color: #424949;
    font-size: 14px;
`;

export const Tag = styled.div`
    float: right;
    margin: 0 0 0 10px;
    color: #697689;
    font-size: 16px;
    font-weight: bold;
`;

export const TL = styled.div`
    width: 560px;
    margin: 0 0 10px 0;
`;

export const CreateUser = styled.div`
    display: inline-block;
    font-size: 16px;
    font-weight: bold;
    margin: 0 0 0 10px;
`;

export const CreateUserA = styled.a`
    color: #697689;
    text-decoration: none;
`;

export const FullHeart = styled.button`
    border: none;
    background: white;
    cursor: pointer;
    position: relative;
    width: 15px;
    height: 13.5px;
    z-index: 1;
    &:before,
    &:after {
        position: absolute;
        content: "";
        left: 7.5px;
        top: 0;
        width: 7.5px;
        height: 12px;
        background: red;
        border-radius: 50px 50px 0 0;
        transform: rotate(-45deg);
        transform-origin: 0 100%;
   }
   &:after {
       left: 0;
       transform: rotate(45deg)
       transform-origin: 100% 100%;
   }
`;

export const EmptyHeart = styled.button`
    border: none;
    background: white;
    cursor: pointer;
    position: relative;
    width: 15px;
    height: 13.5px;
    z-index: 1;
    &:before,
    &:after {
        position: absolute;
        content: "";
        left: 7.5px;
        top: 0;
        width: 7.5px;
        height: 12px;
        background: black;
        border-radius: 50px 50px 0 0;
        transform: rotate(-45deg);
        transform-origin: 0 100%;
   }
   &:after {
       left: 0;
       transform: rotate(45deg)
       transform-origin: 100% 100%;
   }
`;

//Reply
export const Replies = styled.div`
    margin: 0 0 0 10px;
`;

export const ReplyUser = styled.span`
    color: #697689;
    font-size: 14px;
    font-weight: bold;
`;

export const ReplyText = styled.span`
    color: #555555;
    font-size: 12px;
    margin: 0 0 0 7px;
    word-wrap: break-word;
    overflow-wrap: break-word;
`;

export const ReplyLike = styled.span`
    color: #616A6B;
    font-size: 14px;
    margin: 0 0 0 10px;
`;

export const ReplyFullHeart = styled.button`
    border: none;
    background: white;
    cursor: pointer;
    position: relative;
    width: 13px;
    height: 11.7px;
    z-index: 1;
    &:before,
    &:after {
        position: absolute;
        content: "";
        left: 6.5px;
        top: 0;
        width: 6.5px;
        height: 10.4px;
        background: red;
        border-radius: 50px 50px 0 0;
        transform: rotate(-45deg);
        transform-origin: 0 100%;
   }
   &:after {
       left: 0;
       transform: rotate(45deg)
       transform-origin: 100% 100%;
   }
`;

export const ReplyEmptyHeart = styled.button`
    border: none;
    background: white;
    cursor: pointer;
    position: relative;
    width: 13px;
    height: 11.7px;
    z-index: 1;
    &:before,
    &:after {
        position: absolute;
        content: "";
        left: 6.5px;
        top: 0;
        width: 6.5px;
        height: 10.4px;
        background: black;
        border-radius: 50px 50px 0 0;
        transform: rotate(-45deg);
        transform-origin: 0 100%;
   }
   &:after {
       left: 0;
       transform: rotate(45deg)
       transform-origin: 100% 100%;
   }
`;

export const X = styled.button`
    color: #888888;
    border: 0;
    background: white;
    font-weight: bold;
    text-align: center;
    font-size: 12px;
    margin: 0 0 0 3px;
    cursor: pointer;
`;

export const TextArea2 = styled.textarea`
    margin: 1% 0 0 10px;
    width: 470px;
    display: inline-block;
    font-family: PT sans;
    font-size: 14px;
    height: 30px
    color: #697689;
    resize: none;
    padding: 2px;
    border-radius: 5px;
    border: 1px solid #DDDDDD;
    &::placeholder {
        font-family: PT Sans;
        font-size: 14px;
    }
`;

export const Button2 = styled.button`
    background: white;
    border: 1px solid #DDDDDD
    padding: 8px;
    color: #58D68D;
    fontSize: 14px;
    cursor: pointer;
    height: 35px;
    border-radius: 5px;
    margin: 1% 0 0 2%;
    display: inline-block;
    z-index: 1;
    position: relative;
    top: -14px;
    &:hover {
        background: #EEEEEE;
    }
`;

export const Button3 = styled.button`
    background: white;
    border: 1px solid #DDDDDD;
    padding: 8px;
    color: #58D68D;
    font-size: 14px;
    cursor: pointer;
    border-radius: 5px;
    width: 100px;
    &:hover {
        background: #EEEEEE;
    }
`;

//follow
export const FollowContainer = styled.div`
    display: inline-block;
    vertical-align: top;
    width: 20%;
`;

export const FollowBox = styled.div`
    background: white;
    border: 1px solid #CCD1D1;
    height: 240px;
    overflow: hidden;
    border-radius: 5px;
    width: 240px;
    position: fixed;
    top: 120px;
`;

export const FollowBox2 = styled.div`
    background: white;
    border: 1px solid #CCD1D1;
    height: 341px;
    overflow: auto;
    border-radius: 5px;
    width: 240px;
    position: fixed;
    top: 380px;
`;

export const BoxTitle = styled.div`
    font-size: 15px;
    color: #697689;
    font-weight: bold;
    margin: 0 6px 0 6px;
    z-index: 1;
    border-bottom: 1px solid #EEEEEE
    text-align: center;
    padding: 10px;
`;

export const BoxImage = styled.img`
    border-radius: 50%;
    margin: 0 2% 0 0;
    width: 23px;
    height: 23px
    background-position: center center
    background-size: cover;
`;

export const BoxTab = styled.div`
    font-size: 15px;
    color: #697689;
    border-bottom: 1px solid #EEEEEE;
    padding: 10px;
    &:hover {
        background: #EEEEEE;
    }
`;

export const FollowName = styled.div`
    display: inline-block;
    margin-left: 3px;
    position: relative;
    top: -5.5px;
    font-family: PT Sans;
`;

export const BoxA = styled.button`
    border: 0;
    background: white;
    cursor: pointer;
    width: 100%;
    text-align: left;
`;
