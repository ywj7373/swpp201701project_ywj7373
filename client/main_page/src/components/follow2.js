import React from 'react'
import { BoxA, BoxTab, BoxImage, FollowName } from './css'

const Follow2 = ({id, username, image, newProfile}) => {
    const getImage = () => {
        if (image === "") {
            return <BoxImage src="https://museum.wales/media/40374/thumb_480/empty-profile-grey.jpg" alt="User image" />
        }
        else {
            return <BoxImage src={image} alt="User image" />
        }
    }
    
    const clickFollow = () => {
        newProfile(id)
    }

    const followId = "click_follow_" + id

    return (
        <BoxA id={followId} type="submit" onClick={clickFollow}>
            <BoxTab>
                {getImage()}
                <FollowName>{username}</FollowName>
            </BoxTab>
        </BoxA>
    )
}

export default Follow2
