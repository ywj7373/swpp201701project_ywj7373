import React from 'react'
import Follow2 from './follow2'
import { FollowBox2, BoxTitle } from './css'

const Following = ({following, newProfile}) => {
    const renderFollow = (following) => {
        if (following.length > 0) {
            return following.map((follow) => {
                return (
                    <Follow2 key={follow.id} {...follow}
                        newProfile={(a) => newProfile(a)}
                    />
                )
            })
        }
    }
    
    return (
        <FollowBox2>
            <BoxTitle>My Following List</BoxTitle>
            {renderFollow(following)}
        </FollowBox2>
    )
}

export default Following
