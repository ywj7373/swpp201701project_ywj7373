import React from 'react'
import { User, Info, FormBox, Image, Username, UsernameA, Follow, Title, Textarea, Button, Select } from './css'

const PostForm = ({ username, addPost, following, image, tagList }) => {
    let text
    let title
    let tag

    const onPost = () => {
        if (text !== undefined && title !== undefined && tag !== undefined && text.value !== "") {
            let i = 0
            let tagId = null
            
            while (i < tagList.length) {
                if (tagList[i].name === tag.value) {
                    tagId = tagList[i].id
                }
                i++
            }

            addPost(text.value, title.value, tagId)
            text.value = ''
            title.value = ''
        }
    }

    const getFollowingCount = () => {
        let m = following
        if (m !== undefined) {
            return m.length
        }
        else {
            return 0
        }
    }
    
    const getImage = () => {
	    if (image === '') {
	        return <Image src="https://museum.wales/media/40374/thumb_480/empty-profile-grey.jpg" alt="User Image"/>
	    }
	    else {
	        return <Image src={image}/>
	    }
    }

    const selectTag = () => {
        if (tagList.length > 0) {
            return (
                <Select innerRef={node => {tag = node;}}>
                    <option disabled="disabled" selected="selected">
                        Please select a tag</option>
                    <option>{tagList[0].name}</option>
                    <option>{tagList[1].name}</option>
		            <option>{tagList[2].name}</option>
                    <option>{tagList[3].name}</option>
		            <option>{tagList[4].name}</option>
                    <option>{tagList[5].name}</option>
		            <option>{tagList[6].name}</option>
                    <option>{tagList[7].name}</option>
		            <option>{tagList[8].name}</option>
                    <option>{tagList[9].name}</option>
                </Select>
            )
        }
    }

    return (
        <FormBox className="addPost">
     	    <User>    
     	        {getImage()}       
                <Info>
                    <Username className="username">
                        <UsernameA href="/profile" id="click_username">
                            {username}
                        </UsernameA>
                    </Username>
                    <Follow className="following">
                        Following {getFollowingCount()}
                    </Follow>
                </Info>
            </User>
            <Title type="text" id="post_title" placeholder="Write Title..."
                innerRef={node => {title = node;}} />
            <div className="textarea">
                <Textarea id="post_text" placeholder="Write Contents..." 
                    innerRef={node => {text = node;}} />
            </div>
            <div>
                {selectTag()}
                <Button id="post_button" type="submit" onClick={onPost}>
                    Post
                </Button>
            </div>
        </FormBox>
    )
}

export default PostForm
