import React from 'react'
import PostForm from '../containers/post_form'
import PostsList from '../containers/post_list'
import Navbar from '../containers/navbar'
import Followers from '../containers/followers'
import Following from '../containers/following'
import { Screen, Main, MainContainer, FollowContainer } from './css'

const App = () => {
    return (
        <Screen>
            <Navbar />
            <Main>
                <MainContainer>
                    <PostForm />
                    <PostsList />
                </MainContainer>
                <FollowContainer>
                    <Followers />
                    <Following />
                </FollowContainer>
            </Main>
        </Screen>
    );
}

export default App
