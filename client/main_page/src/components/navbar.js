import React from 'react'
import { UL, A, A2, LI, LI2, Logo, Vertical } from './css'

const Navbar = ({ logout }) => {
    return (
        <UL className="navigation bar">
		    <Logo><A2 href='/' id="main_button">Folivora</A2></Logo>
		    <LI>
		        <A href='/chat' id="chat_button">Chat</A>
		    </LI>
		    <LI><Vertical>|</Vertical></LI>
		    <LI>
		        <A href='/profile' id="profile_button">My Profile</A>
            </LI>
            <LI><Vertical>|</Vertical></LI>
		    <LI2>
                <A id="logout_button"
                    onClick={logout}>Logout</A>
            </LI2>
        </UL>
    )
}

export default Navbar
