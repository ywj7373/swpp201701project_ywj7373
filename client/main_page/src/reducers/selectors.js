export const createNewInitialState = () => {
    return {
        userId: 0,
        username: '',
        image: '',
        nextUrl: '/api/post',
        loadingMore: false,
        fullList: false,
        lid: 0,
        posts: [],
        tagList: [],
        following: [],
        recFollows: []
    }
}

const initialState = createNewInitialState()

export default initialState
