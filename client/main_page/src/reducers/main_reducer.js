import initialState from './selectors'

const posts = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_USER_INFO':
            return {
                ...state,
                userId: action.id,
                username: action.username,
                image: action.image
            }

        case 'SET_IS_EMPTY':
            let isFull = false

            if (action.next === null && action.posts.length === 0) {
                isFull = true
            }

            return {
                ...state,
                fullList: isFull
            }

        case 'SET_LAST_POST_ID':
            let list = action.posts
            let lastId = 0

            if (list.length > 0) {
                lastId = list[list.length-1].id
            }

            return {
                ...state,
                lid: lastId
            }

        case 'SET_TAG_LIST':
            return {
                ...state,
                tagList: action.tags
            }

        case 'PUSH_FOLLOWING':
            const follow = {
                id: action.user,
                username: action.username,
                image: action.image
            }

            return {
                ...state,
                following: [
                    ...state.following,
                    follow
                ]
            }

        case 'PUSH_FOLLOWER':
            return {
                ...state,
                recFollows: action.recFollows
            }

        case 'PUSH_POSTS':
            const post = {
                id: action.id,
                user: action.user,
                username: action.username,
                tag: action.tag,
                title: action.title,
                body: action.body,
                created: action.created,
                replies: action.replies,
		        nextReplies: action.nextReplies,
		        nextRepliesNum: action.nextRepliesNum,
			    likes: action.likes
            }

            let isFull_2 = false
            if (action.id === state.lid) {
                isFull_2 = true
            }

            return {
                ...state,
                posts: [
                    ...state.posts,
                    post
                ],
                fullList: isFull_2
            }

        case 'UPDATE_POST_LIST':
            const post_2 = {
                id: action.id,
                user: action.user, 
                username: action.username,
                tag: action.tag,
                title: action.title,
                body: action.body,
                created: action.created,
                replies: action.replies,
                nextReplies: action.nextReplies,
                nextRepliesNum: action.nextRepliesNum,
                likes: action.likes
            }

            if (state.posts.length%10 === 0) {
                state.posts.pop()
            }

            let lastId_2 = state.lid
            let isFull_3 = false

            if (state.posts.length > 0) {
                let last = state.posts[state.posts.length-1]

                if (last.id === state.lid) {
                    isFull_3 = true
                }
            }
            else {
                lastId_2 = post_2.id
                isFull_3 = true
            }

            return Object.assign({}, state, {
		        posts: [
		            post_2,
		            ...state.posts
		        ],
		        fullList: isFull_3,
		        lid: lastId_2
		    })

        case 'PUSH_REPLIES':
            const reply = {
                id: action.rid,
                user: action.user,
                username: action.username,
                created: action.created,
                post: action.post,
                body: action.body,
                like: action.like,
                popular: false
            }

            if (reply.like.length > 1) {
                reply.popular = true
            }

            let list_2 = state.posts
            let index = -1

            for (let i = 0; i < list_2.length; i++) {
                if (list_2[i].id === action.pid) {
                    index = i
                }
            }

            return Object.assign({}, state, {
                posts: state.posts.map((post, i) => {
                    if (i === index) {
                        let num = 0
                        let tmp = state.posts[i].replies
                        if (tmp.length%10 === 0) {
                            num = Math.trunc((tmp.length-1)/10)+1
                        }
                        else {
                            num = Math.trunc(tmp.length/10)+1
                        }

                        return Object.assign({}, post, {
                            replies: [
                                ...state.posts[i].replies,
                                reply
                            ],
                            nextReplies: action.nextReplies,
                            nextRepliesNum: num
                        })
                    }
                    return post
                })
            })

        case 'UPDATE_REPLY_LIST':
	        const reply_2 = {
	            id: action.rid,
	            user: action.user,
	            username: action.username,
	            created: action.created,
	            post: action.post,
	            body: action.body,
	            like: action.like,
	            popular: false
	        }

	        let list_3 = state.posts
            let index_2 = -1

            for (let i = 0; i < list_3.length; i++) {
                if (list_3[i].id === action.id) {
                    index_2 = i
                }
            }

            return Object.assign({}, state, {
                posts: state.posts.map((post, i) => {
		            let replies = state.posts[i].replies
                    if (i === index_2 && replies.length === 0) {
                        return Object.assign({}, post, {
                            replies: [
                                ...replies,
				                reply_2
                            ],
                        })
                    }
		            else if (i === index_2 && replies.length%10 !== 0) {
			            return Object.assign({}, post, {
			                replies: [
                                ...replies,
				                reply_2
                            ],
                        })
                    }
                    else if (i === index_2 && state.posts[i].nextReplies === null) {
                        return Object.assign({}, post, {
                            nextReplies: action.nextReplies
                        })
                    }
                    return post
                })
            })

        case 'DELETE_REPLY_FROM_LIST':
	        let list_5 = state.posts
            let index_4 = -1

            for (let i = 0; i < list_5.length; i++) {
                if (list_5[i].id === action.pid) {
                    index_4 = i
                }
            }

            return Object.assign({}, state, {
                posts: state.posts.map((post, i) => {
                    if (i === index_4) {
                        let tmp = state.posts[i].replies
                        let k = 0
                        while (k < tmp.length) {
                            if (tmp[k].id === action.id) {
                                tmp.splice(k, 1)
                            }
                            k++
                        }

                        let num = 0
                        if (tmp.length%10 == 0) {
                            num = Math.trunc((tmp.length-1)/10)+1
                        }
                        else {
                            num = Math.trunc(tmp.length/10)+1
                        }

                        if (action.nextReplies !== null) {
                            tmp.push(action.newReply)
                        }
                        else if (action.nextReplies === null &&
                            action.newReply !== null &&
                            action.newReply.id !== tmp[tmp.length-1].id) {
                            tmp.push(action.newReply)
                        }

                        return Object.assign({}, post, {
                            replies: tmp,
                            nextReplies: action.nextReplies,
                            nextRepliesNum: num
                        })
                    }
                    return post
                })
            })

        case 'UPDATE_REPLY_LIKE_LIST':
            let list_6 = state.posts
            let index_5 = -1
            let index_6 = -1

            for (let i = 0; i < list_6.length; i++) {
                if (list_6[i].id === action.pid) {
                    index_5 = i
                    let list_7 = state.posts[i].replies
                    for (let j = 0; j < list_7.length; j++) {
                        if (list_7[j].id === action.rid) {
                            index_6 = j
                        }
                    }
                }
            }
            
            return Object.assign({}, state, {
                posts: state.posts.map((post, i) => {
                    if (i === index_5) {
                        return Object.assign({}, post, {
                            replies: state.posts[i].replies.map((reply, j) => {
                                if (j === index_6) {
                                    let popu = false
                                    if (action.like.length > 1) {
                                        popu = true
                                    }
                                    return Object.assign({}, reply, {
                                        like: action.like,
                                        popular: popu
                                    })
                                }
                                return reply
                            })
                        })
                    }
                    return post
                })
            })
        
        case 'UPDATE_LIKE_LIST':
            let list_4 = state.posts
            let index_3 = -1

            for (let i = 0; i < list_4.length; i++) {
                if (list_4[i].id === action.id) {
                    index_3 = i
                }
            }

            return Object.assign({}, state, {
                posts: state.posts.map((post, i) => {
                    if (i === index_3) {
                        return Object.assign({}, post, {
                            likes: action.likes
                        })
                    }
                    return post
                })
            })

        case 'UPDATE_NEXT_URL':
            return {
                ...state,
                nextUrl: action.url
            }

        case 'TOGGLE_LOAD':
            return {
                ...state,
                loadingMore: action.toggle
            }

        default:
            return state
    }
}

export default posts
