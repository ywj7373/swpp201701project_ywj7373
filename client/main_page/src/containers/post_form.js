import { connect } from 'react-redux'
import PostForm from '../components/post_form'
import { addPost, gotoProfile } from '../actions/actions'

const mapStateToProps = (state) => {
    return {
        username: state.username,
        following: state.following,
        image: state.image,
        tagList: state.tagList
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addPost: (text, titleText, tag) => {
            dispatch(addPost(text, titleText, tag))
        },

        gotoProfile: () => {
            dispatch(gotoProfile())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostForm)
