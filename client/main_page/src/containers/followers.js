import { connect } from 'react-redux'
import Followers from '../components/followers'
import { addFollow } from '../actions/actions'

const mapStateToProps = (state) => {
    return {
        recFollows: state.recFollows
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addFollow: (fid) => {
            dispatch(addFollow(fid))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Followers)
