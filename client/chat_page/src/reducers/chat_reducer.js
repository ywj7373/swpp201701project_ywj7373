import initialState from './selectors'

const chat = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_USER_INFO':
            return {
                ...state,
                userId: action.id,
                username: action.username,
                image: action.image
            }

        case 'PUSH_FOLLOWING':
            const follow = {
                id: action.user,
                username: action.username,
                image: action.image
            }

            return {
                ...state,
                following: [
                    ...state.following,
                    follow
                ]
            }
        
        case 'SET_FRIEND_INFO':
            return {
                ...state,
                friendUserId: action.id,
                friendUsername: action.username,
                friendImage: action.image
            }

        case 'PUSH_MESSAGES':
            var arr = action.toData
            var combine = arr.concat(action.fromData)
            combine.sort(function(a,b) {
                if (a.id > b.id) {
                    return 1
                }
                if (a.id < b.id) {
                    return -1
                }
                return 0
            })

            return {
                ...state,
                messages: combine
            }

        default:
            return state
    }
}

export default chat
