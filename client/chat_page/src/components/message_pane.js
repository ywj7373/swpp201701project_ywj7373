import React, { Component } from 'react'
import Message from './message'
import { MessageBox, TextArea, Button } from './css'

class MessagePane extends Component {
    constructor(props) {
        super(props)
        this.state = {
            text: ''
        }
        this.props.getMessage()
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount() {
        setInterval(() => this.refresh(), 500)
    }

    refresh() {
        this.props.getMessage()
        var element = document.getElementById("messageBox")
        var isBottom = (element.scrollHeight - element.clientHeight) <= (element.scrollTop + 50)
        if (isBottom) {
            element.scrollTop = element.scrollHeight
        }
    }

    renderMessages() {
        if (this.props.friendUserId !== 0 && this.props.messages.length > 0) {
            return this.props.messages.map((message) => {
                return (
                    <Message key={message.id} {...message}
                        username={this.props.username}
                        friendUsername={this.props.friendUsername}
                    />
                )
            })
        }
    }

    handleChange(e) {
        e.preventDefault()
        let text = e.target.value
        this.setState({
            text: text
        })
    }

    handleSubmit(e) {
        e.preventDefault()
        if (this.props.friendUserId !== 0) {
            this.props.addMessage(this.state.text, this.props.friendUserId)
            document.getElementById("message_text").value = ""
        }
    }
            
            
    render() {
        return(
            <div className="Messages">
                <MessageBox id="messageBox">
                    <div style={{"padding": "0 0 0 5%"}}>
                        {this.renderMessages()}
                    </div>
                </MessageBox>
                <TextArea id="message_text" placeholder="say something..."
                    onChange={(e) => this.handleChange(e)} />
                <div style={{"display": "inline-block"}}>
                    <Button id="post_button" type="submit"
                        onClick={(e) => this.handleSubmit(e)}>Enter</Button>
                </div>
            </div>
        )
    }
}

export default MessagePane
