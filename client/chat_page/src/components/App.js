import React from 'react'
import Navbar from '../containers/navbar'
import UserPane from '../containers/user_pane'
import Following from '../containers/following'
import MessagePane from '../containers/message_pane'
import { Screen, Main, UserContainer, MessageContainer } from './css'

const App = () => {
    return (
        <Screen>
            <Navbar />
            <Main>
                <UserContainer>
                    <UserPane />
                    <Following />
                </UserContainer>
                <MessageContainer>
                    <MessagePane />
                </MessageContainer>
            </Main>
        </Screen>
    );
}

export default App
