import React, { Component } from 'react'
import { User, Info, Image, Username, Follow } from './css'

class UserPane extends Component {
    componentWillMount() {
        this.props.updateState()
    }

    getImage() {
        if (this.props.image === '') {
            return <Image src="https://museum.wales/media/40374/thumb_480/empty-profile-grey.jpg" alt="User Image"/>
        }
        else {
            return <Image src={this.props.image}/>
        }
    }

    getFollowingCount() {
        let m = this.props.following
        if (m !== undefined) {
            return m.length
        }
        else {
            return 0
        }
    }
            

    render() {
        return(
            <User>
                {this.getImage()}
                <Info>
                    <Username className="username">
                        {this.props.username}
                    </Username>
                    <Follow className="following">
                        Following {this.getFollowingCount()}
                    </Follow>
                </Info>
            </User>
        )
    }
}

export default UserPane
