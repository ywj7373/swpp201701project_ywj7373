import React from 'react'
import { MessageUser, MessageText } from './css'

const Message = ({user_username, to_username, body, username, friendUsername}) => {
    const toggleMessage = () => {
        if ((user_username == username && to_username == friendUsername) ||
            (user_username == friendUsername && to_username == username)) {
            return (
                <div>
                    <MessageUser>{user_username}</MessageUser>
                    <MessageText>{body}</MessageText>
                </div>
            )
        }
    }

    return (
        <div>
            {toggleMessage()}
        </div>
    )
}

export default Message
