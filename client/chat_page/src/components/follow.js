import React from 'react'
import { BoxA, BoxTab, BoxImage, BoxUser } from './css'

const Follow = ({id, username, image, setFriendInfo}) => {
    const getImage = () => {
        if (image === "") {
            return <BoxImage src="https://museum.wales/media/40374/thumb_480/empty-profile-grey.jpg" alt="User image" />
        }
        else {
            return <BoxImage src={image} alt="User image" />
        }
    }
    
    const clickFollow = () => {
        setFriendInfo(id, username, image)
    }

    const followId = "click_follow_" + id

    return (
        <BoxA id={followId} type="submit" onClick={clickFollow}>
            <BoxTab>
                {getImage()}
                <BoxUser>{username}</BoxUser>
            </BoxTab>
        </BoxA>
    )
}

export default Follow
