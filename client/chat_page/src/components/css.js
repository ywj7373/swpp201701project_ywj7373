import styled from 'styled-components'

//App
export const Screen = styled.div`
    font-family: PT sans
`;

export const Main = styled.div`
    padding: 90px 0 0 0;
    margin: 0 auto;
    max-width: 1200px;
`;

export const UserContainer = styled.div`
    display: inline-block;
    width: 300px;
    margin: 0 3% 0 13%;
`;

export const MessageContainer = styled.div`
    display: inline-block;
    vertical-align: top;
    width: 550px;
    height: 500px;
    padding: 5px;
    border-radius: 5px;
    border: 1px solid #CCD1D1;
    background: white;
`;

//navbar
export const UL = styled.ul`
    list-style-type: none;
    margin: 0;
    padding 20px 50px 20px 0;
    overflow: hidden;
    background-color: white;
    text-align: right;
    height: 50px;
    position: fixed;
    top: 0;
    right: 0;
    left: 0;
    width: 100%;
    border-bottom: 1px solid #CCD1D1;
    z-index: 2;
    box-shadow: 0 2px 3px #CCD1D1;
`;

export const A = styled.a`
    color: #555555;
    font-size: 14px;
    font-weight: bold;
    cursor: pointer;
    text-decoration: none;
    &:hover {
       color: #58D68D;
    }
`;

export const A2 = styled.a`
    text-decoration: none;
    cursor: pointer
    color: #58D68D;
`; 

export const LI2 = styled.li`
    display: inline-block;
    padding: 13px 24% 0 0;
`;

export const Logo = styled.li`
    float: left;
    font-family: lobster two;
    font-size: 40px;
    color: #58D68D;
    padding: 0 0 0 17.5%;
`;

//User Pane
export const User = styled.div`
    height: 150px;
    width: 100%;
    background: white;
    border: 1px solid #CCD1D1;
    border-radius: 5px;
    margin: 0 0 3% 0;

`;

export const Info = styled.div`
    width: 50%;
    height: 80px;
    margin: 12% 0 0 4%;
    float: left;
`;

export const Image = styled.img`
    border-radius: 50%;
    margin: 6.5% 0 0 3%;
    width: 110px;
    height: 110px;
    background-position: center center;
    background-size: cover;
    float: left;
`;

export const Username = styled.span`
    font-size: 35px;
    color: #697689;
    font-weight: bold;
    z-index: 1;
`;

export const Follow = styled.div`
    font-size: 15px;
    color: #697689;
    margin: 10px 0 0 2px;
`;

//Message
export const MessageBox = styled.div`
    padding: 2px;
    width: 100%;
    height: 400px;
    overflow: auto;
`;

export const MessageUser = styled.span`
    color: #697689;
    font-size: 16px;
    font-weight: bold;
`;

export const MessageText = styled.span`
    color: #555555;
    font-size: 14px;
    margin: 0 0 0 5px;
    word-wrap: break-word;
    overflow-wrap: break-word;
`;

export const TextArea = styled.textarea`
    width: 80%;
    height: 30px;
    font-family: PT sans;
    font-size: 14px;
    color: #555555;
    display: inline-block;
    border: 1px solid #DDDDDD;
    border-radius: 5px;
    margin: 5% 5px 0 5%;
    resize: none;
    &::placeholder {
        font-family: PT sans;
        font-size: 14px;
    }
`;

export const Button = styled.button`
    background: white;
    border: 1px solid #DDDDDD;
    padding: 8px;
    color: #58D68D;
    fontSize: 14px;
    cursor: pointer;
    border-radius: 5px;
    margin: 1% 0 0 0;
    position: relative;
    top: -14px
    z-index: 1;
    &:hover {
        background: #EEEEEE;
    }
`;

//follow
export const FollowBox = styled.div`
    background: white;
    border: 1px solid #DDDDDD;
    height: 340px;
    overflow: auto;
    border-radius: 5px;
    width: 100%;
    padding: 10px
`;

export const BoxTitle = styled.div`
    font-size: 15px;
    color: #698689;
    font-weight: bold;
    padding: 0 10px 10px 10px;
    border-bottom: 1px solid #CCD1D1;
    z-index: 1;
    text-align: center;
`;

export const BoxImage = styled.img`
    border-radius: 50%;
    margin: 0 2% 0 0;
    width: 23px;
    height: 23px
    background-position: center center
    background-size: cover;
    display: inline-block;
`;

export const BoxTab = styled.div`
    font-size: 15px;
    border-bottom: 1px solid #CCD1D1;
    padding: 10px;
    text-align: left;
    &:hover {
        background: #EEEEEE;
    }
`;

export const BoxA = styled.button`
    background: white;
    border: none;
    width: 100%;
    cursor: pointer;
`;

export const BoxUser = styled.div`
    display: inline-block;
    color: #697689;
    margin-left: 3px;
    position: relative;
    top: -5.5px;
`;
