import $ from 'jquery'

const logout_url = "/api/logout/"
const user_url = "/api/user/"
const get_user_url = (id) => {
    return user_url + id + "/"
}
const follow_url = "/api/follow/"
const message_url = "/api/message/"
const message_to_url = "/api/message/to/"
const message_from_url = "/api/message/from/"

var token = document.getElementById("csrftoken").value

//initial actions updating state
export function updateState() {
    return dispatch => {
       dispatch(getUserInfo())
       dispatch(getFollowingList())
    }
}

export function getUserInfo() {
    const request = $.get(user_url)

    return dispatch => {
        request.done(function(data) {
            if (data.length === 1) {
                let user = data[0]
                dispatch(setUserInfo(user.id, user.username, user.image))
            }
         })
    }
}

export const setUserInfo = (id, username, image) => {
    return {
        type: 'SET_USER_INFO',
        id,
        username,
        image
    }
}

export function getFollowingList() {
    const request = $.get(follow_url)

    return dispatch => {
        request.done(function(data) {
            let len = data.length
            let i = 0
            while (i < len) {
                dispatch(getFollowUsername(data[i].follow))
                i++
            }
        })
    }
}

export function getFollowUsername(id) {
    const request = $.get(get_user_url(id))

    return dispatch => {
        request.done(function(data) {
            dispatch(pushFollowing(id, data.username, data.image))
        })
    }
}

export const pushFollowing = (user, username, image) => {
    return {
        type: 'PUSH_FOLLOWING',
        user,
        username,
        image
    }
}

//friend info actions
export const setFriendInfo = (id, username, image) => {
    return {
        type: 'SET_FRIEND_INFO',
        id,
        username,
        image
    }
}

//message actions
export function getMessage() {
    return dispatch => {
        dispatch(getMessageTo(message_to_url))
    }
}

export function getMessageTo(url) {
    const request = $.get(url)

    return dispatch => {
        request.done(function(data) {
            dispatch(getMessageFrom(message_from_url, data))
        })
    }
}

export function getMessageFrom(url, toData) {
    const request = $.get(url)

    return dispatch => {
        request.done(function(data) {
            dispatch(pushMessages(toData, data))
        })
    }
}

export const pushMessages = (toData, fromData) => {
    return {
        type: 'PUSH_MESSAGES',
        toData,
        fromData
    }
}

export function addMessage(text, id) {
    const request = $.post(message_url, {
        to: id,
        body: text,
        csrfmiddlewaretoken: token
    })

    return dispatch => {
    }
}

//page actions
export function logout() {
    const request = $.post(logout_url, {
        csrfmiddlewaretoken: token
    })

    return dispatch => {
        request.done(response => {
            window.location.replace("/login/")
        })
    }
}
