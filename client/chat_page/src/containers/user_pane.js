import { connect } from 'react-redux'
import UserPane from '../components/user_pane'
import { updateState } from '../actions/actions'

const mapStateToProps = (state) => {
    return {
        username: state.username,
        image: state.image,
        following: state.following
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateState: () => {
            dispatch(updateState())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserPane)
