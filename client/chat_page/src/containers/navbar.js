import { connect } from 'react-redux'
import Navbar from '../components/navbar'
import { logout } from '../actions/actions'

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => {
            dispatch(logout())
        }
    }
}

export default connect(null, mapDispatchToProps)(Navbar)
