import { connect } from 'react-redux'
import Following from '../components/following'
import { setFriendInfo } from '../actions/actions'

const mapStateToProps = (state) => {
    return {
        following: state.following
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setFriendInfo: (id, username, image) => {
            dispatch(setFriendInfo(id, username, image))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Following)
