import { connect } from 'react-redux'
import MessagePane from '../components/message_pane'
import { getMessage, addMessage } from '../actions/actions'

const mapStateToProps = (state) => {
    return {
        username: state.username,
        friendUsername: state.friendUsername,
        friendUserId: state.friendUserId,
        messages: state.messages
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getMessage: () => {
            dispatch(getMessage())
        },

        addMessage: (text, id) => {
            dispatch(addMessage(text, id))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MessagePane)
