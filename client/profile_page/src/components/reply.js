import React from 'react'
import { ReplyUser, ReplyText, ReplyLike, ReplyFullHeart, ReplyEmptyHeart, X } from './css'

const Reply = ({ id, user, username, created, post, body, like, popular, num, mainUserId, deleteReply, addReplyLike, deleteReplyLike }) => {
    let likeId = 0

    const deleteReplyId = id + "_delete_reply"
    const replyLikeId = id + "_reply_like"
    const replyDislikeId = id + "_reply_dislike"

    const clickDelete = () => {
        deleteReply(post, id, num)
    }
    
    const getLikeCount = () => {
        let l = like
        if (l !== undefined) {
            return l.length
        }
        else {
            return 0
        }
    }
    
    const clickLike = () => {
        addReplyLike(post, id)
    }

    const clickDislike = () => {
        deleteReplyLike(post, id, likeId)
    }

    const toggleHeart = () => {
        let len = like.length
        let i = 0
        while (i < len) {
            if (like[i].user === mainUserId) {
                likeId = like[i].id
                return <ReplyFullHeart style={popular?{"background": "#95E3E3"}:{}}
                        id={replyDislikeId} type="submit" 
                        onClick={clickDislike}></ReplyFullHeart>
            }
            i++
        }

        return <ReplyEmptyHeart style={popular?{"background": "#95E3E3"}:{}} 
                id={replyLikeId} type="submit" 
                onClick={clickLike}></ReplyEmptyHeart>
    }

    const toggleDelete = () => {
        if (user === mainUserId) {
            return <X style={popular?{"background": "#95E3E3"}:{}} 
                    id={deleteReplyId} type="submit" onClick={clickDelete}>X</X>
        }
    }

    return (
        <div style={popular?{"background": "#95E3E3"}:{}}>
            <ReplyUser>{username}</ReplyUser>
            <ReplyText>{body}</ReplyText>
            <ReplyLike>
                {toggleHeart()}
                <span style={{"margin": "0 0 0 3px"}}>
                   {getLikeCount()}
                </span>
            </ReplyLike>
            {toggleDelete()}
        </div>
    )
}

export default Reply
