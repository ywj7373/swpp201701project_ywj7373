import styled from 'styled-components'

//App
export const Screen = styled.div`
    font-family: PT sans
`;

export const Main = styled.div`
    padding: 90px 0 0 0;
    margin: 0 auto;
    max-width: 1200px;
`;

export const MainContainer = styled.div`
    width: 100%;
    margin: 0 0 0 13%;
`;

export const Posts = styled.div`
    display: inline-block;
    margin: 0 0.6% 0 0;
    width: 564px;
`;

export const FollowBox = styled.div`
    display: inline-block;
    width: 283px;
    background: white;
    border: 1px solid #DDDDDD;
    border-radius: 5px;
    padding: 10px;
    vertical-align: top;
    height: 340px;
    overflow: auto;
`;

//navbar
export const UL = styled.ul`
    list-style-type: none;
    margin: 0;
    padding 20px 50px 20px 0;
    overflow: hidden;
    background-color: white;
    text-align: right;
    height: 50px;
    position: fixed;
    top: 0;
    right: 0;
    left: 0;
    width: 100%;
    border-bottom: 1px solid #CCD1D1;
    z-index: 2;
    box-shadow: 0 2px 3px #CCD1D1
`;

export const A = styled.a`
    color: #555555;
    font-size: 14px;
    font-weight: bold
    text-decoration: none;
    cursor: pointer;
    &:hover {
       color: #58D68D;
    }
`;

export const A2 = styled.a`
    text-decoration: none;
    color: #58D68D;
    cursor: pointer;
`;

export const LI = styled.li`
    display: inline-block;
    padding: 13px 15px 0 0;
`;

export const LI2 = styled.li`
    display: inline-block;
    padding: 13px 24% 0 0;
`;

export const Logo = styled.li`
    float: left;
    font-family: lobster two;
    font-size: 40px;
    color: #58D68D;
    padding: 0 0 0 17.5%;
`;

//user
export const User = styled.div`
    border-bottom: 1px solid #DDDDDD;
    border-radius: 5px
    background: white;
    margin: 0 0 2% 13%;
    width: 875px;
    height: 130px;
`;

export const Image = styled.img`
    border-radius: 50%;
    margin: 1.5% 0 1% 4%;
    float: left;
    width: 100px;
    height: 100px;
    background-position: center center;
    background-size: cover;
`;

export const Info = styled.div`
    width: 50%;
    height: 120px;
    margin: 3% 0 0 3%;
    float: left;
`;

export const AA = styled.a`
    color: #697689;
    text-decoration: none;
`;

export const Username = styled.div`
    font-size: 35px;
    font-weight: bold;
    z-index: 1;
`;

export const Follow = styled.div`
    font-size: 15px;
    color: #697689;
`;

//Followers
export const BoxA = styled.a`
    text-decoration: none;
    color: #424949;
`;

export const BoxTitle = styled.div`
    font-size: 15px;
    color: #698689;
    font-weight: bold;
    padding: 10px;
    border-bottom: 1px solid #CCD1D1
    z-index: 1;
    text-align: center;
`;

export const BoxTab = styled.div`
    font-size: 15px;
    border-bottom: 1px solid #CCD1D1;
    padding: 10px;
    &:hover {
        background: #EEEEEE;
    }
`;

export const BoxImage = styled.img`
    border-radius: 50%;
    margin: 0 2% 0 0;
    width: 23px;
    height: 23px
    background-position: center center;
    background-size: cover;
    display: inline-block;
`;

export const BoxUser = styled.div`
    display: inline-block;
    color: #697689;
    margin-left: 3px;
    position: relative;
    top: -5.5px;
`;

export const Button = styled.button`
    background: white;
    border: 1px solid #DDDDDD;
    padding: 8px;
    color: #58D68D;
    fontSize: 14px;
    cursor: pointer;
    border-radius: 5px;
    width: 100px;
    &:hover {
        background: #EEEEEE;
    }
`;

//Post list
export const Box = styled.div`
    width: 520px;
    background-color: white;
    border: 1px solid #DDDDDD;
    border-radius: 5px;
    padding: 20px 20px 0 20px;
    margin: 0 0 3% 0;
`;

export const Head = styled.div`
    width: 500px;
    font-size: 18px;
    margin: 0 0 1% 10px;
    color: #444444;
    word-wrap: break-word;
`;

export const Body = styled.div`
    width: 500px;
    font-size: 14px;
    margin: 0 0 1% 10px;
    color: #555555;
    word-wrap: break-word;
`;

export const Like = styled.div`
    margin: 0 0 2% 2%;
    color: #424949;
    font-size: 14px;
`;

export const Tag = styled.div`
    float: right;
    margin: 0 0 0 10px;
    color: #697689;
    font-size: 14px;
    font-weight: bold;
`;

export const TL = styled.div`
    width: 500px;
    margin: 0 0 10px 0;
`;

export const CreateUser = styled.div`
    font-weight: bold;
    font-size: 16px;
    display: inline-block;
    margin: 0 0 0 10px;
`;

export const CreateUserA = styled.a`
    color: #697689;
    text-decoration: none;
`;

export const FullHeart = styled.button`
    border: none;
    background: white;
    cursor: pointer;
    position: relative;
    width: 15px;
    height: 13.5px;
    z-index: 1;
    &:before,
    &:after {
        position: absolute;
        content: "";
        left: 7.5px;
        top: 0;
        width: 7.5px;
        height: 12px;
        background: red;
        border-radius: 50px 50px 0 0;
        transform: rotate(-45deg);
        transform-origin: 0 100%;
   }
   &:after {
       left: 0;
       transform: rotate(45deg)
       transform-origin: 100% 100%;
   }
`;

export const EmptyHeart = styled.button`
    border: none;
    background: white;
    cursor: pointer;
    position: relative;
    width: 15px;
    height: 13.5px;
    z-index: 1;
    &:before,
    &:after {
        position: absolute;
        content: "";
        left: 7.5px;
        top: 0;
        width: 7.5px;
        height: 12px;
        background: black;
        border-radius: 50px 50px 0 0;
        transform: rotate(-45deg);
        transform-origin: 0 100%;
   }
   &:after {
       left: 0;
       transform: rotate(45deg)
       transform-origin: 100% 100%;
   }
`;

export const Hr = styled.div`
    background: #EEEEEE;
    height: 1px;
    margin: 0 5% 1% 0;
`;

//Reply
export const Replies = styled.div`
    margin: 0 0 0 10px;
`;

export const ReplyUser = styled.span`
    color: #697689;
    font-size: 14px;
    font-weight: bold;
`;

export const ReplyText = styled.span`
    color: #555555;
    font-size: 12px;
    margin: 0 0 0 7px;
    word-wrap: break-word;
    overflow-wrap: break-word;
`;

export const ReplyLike = styled.span`
    color: #616A6B;
    font-size: 14px;
    margin: 0 0 0 10px;
`;

export const ReplyFullHeart = styled.button`
    border: none;
    background: white;
    cursor: pointer;
    position: relative;
    width: 13px;
    height: 11.7px;
    z-index: 1;
    &:before,
    &:after {
        position: absolute;
        content: "";
        left: 6.5px;
        top: 0;
        width: 6.5px;
        height: 10.4px;
        background: red;
        border-radius: 50px 50px 0 0;
        transform: rotate(-45deg);
        transform-origin: 0 100%;
   }
   &:after {
       left: 0;
       transform: rotate(45deg)
       transform-origin: 100% 100%;
   }
`;

export const ReplyEmptyHeart = styled.button`
    border: none;
    background: white;
    cursor: pointer;
    position: relative;
    width: 13px;
    height: 11.7px;
    z-index: 1;
    &:before,
    &:after {
        position: absolute;
        content: "";
        left: 6.5px;
        top: 0;
        width: 6.5px;
        height: 10.4px;
        background: black;
        border-radius: 50px 50px 0 0;
        transform: rotate(-45deg);
        transform-origin: 0 100%;
   }
   &:after {
       left: 0;
       transform: rotate(45deg)
       transform-origin: 100% 100%;
   }
`;

export const X = styled.button`
    color: #888888;
    border: 0;
    background: white;
    font-weight: bold;
    text-align: center;
    font-size: 12px;
    margin: 0 0 0 3px;
    cursor: pointer;
`;

export const X2 = styled.button`
    color: #888888;
    border: 0;
    background: white;
    font-weight: bold;
    text-align: center;
    font-size: 12px;
    margin: 0 0 1% 93%;
    cursor: pointer;
`;

export const TextArea2 = styled.textarea`
    margin: 1% 0 0 10px;
    width: 430px;
    display: inline-block;
    font-family: PT Sans;
    font-size: 14px;
    color: #697689;
    resize: none;
    padding: 2px;
    height: 30px;
    border: 1px solid #DDDDDD
    border-radius: 5px;
    &::placeholder {
        font-family: PT Sans;
        font-size: 14px;
    }
`;

export const Button2 = styled.button`
    background: white;
    border: 1px solid #DDDDDD;
    padding: 8px;
    color: #58D68D;
    fontSize: 14px;
    cursor: pointer;
    height: 35px'
    border-radius: 5px;
    margin: 1% 0 0 2%;
    display: inline-block;
    z-index: 1;
    position: relative;
    top: -14px;
    &:hover {
        background: #EEEEEE;
    }
`;
