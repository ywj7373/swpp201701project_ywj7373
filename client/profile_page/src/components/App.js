import React from 'react'
import Navbar from '../containers/navbar'
import UserInfo from '../containers/user'
import PostsList from '../containers/post_list'
import Followers from '../containers/followers'
import { Screen, Main, MainContainer, Posts } from './css'

const App = () => {
    return (
        <Screen>
            <Navbar />
            <Main>
                <UserInfo />
                <MainContainer>
                    <Posts>
                        <PostsList />
                    </Posts>
                    <Followers />
                </MainContainer>
            </Main>
        </Screen>
    );
}

export default App
