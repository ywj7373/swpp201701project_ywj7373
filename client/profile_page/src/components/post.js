import React from 'react'
import Reply from './reply'
import { Box, Head, Body, Like, TL, Tag, CreateUser, CreateUserA, FullHeart, EmptyHeart, Hr, Replies, TextArea2, Button, Button2, X, X2 } from './css'

const Post = ({mainUserId, userId, nextUrlNum, id, user, username, title, body, created, tag, tagList, replies, likes, nextReplies, nextRepliesNum, addReply, loadReply, addLike, deleteLike, deleteReply, addReplyLike, deleteReplyLike, deletePost }) => {
    let text
    let likeId = 0
    let tagName = ""
    
    const loadReplyId = id + "_loadReply_button"
    const dislikeId = id + "_dislike_button"
    const clickLikeId = id + "_like_button"
    const deletePostId = id + "_delete_button"
    const replyTextId = id + "_reply_text"
    const replyButtonId = id + "_reply_button"

    const toggleTitle = () => {
        if (title !== "") {
            return <Head className="title">{title}</Head>
        }
    }

    const getLikeCount = () => {
        let m = likes
        if (m !== undefined) {
            return m.length
        }
        else {
            return 0
        }
    }
    
    const clickReply = () => {
        if (text !== undefined && text.value !== "") {
            addReply(id, text.value)
            text.value = ''
        }
    }

    const clickLike = () => {
        addLike(id)
    }

    const clickDislike = () => {
        deleteLike(id, likeId)
    }
    
    const clickLoad = () => {
        loadReply(id, nextReplies)
    }

    const load = () => {
        if (nextReplies !== null) {
            return <Button id={loadReplyId} 
                type="submit" onClick={clickLoad}>Load More</Button>
        }
    }
    
    const toggleHeart = () => {
        let len = likes.length
        let i = 0
        while (i < len) {
            if (likes[i].user === mainUserId) {
                likeId = likes[i].id
                return <FullHeart id={dislikeId} 
                    type="submit" onClick={clickDislike}></FullHeart>
            }
            i++
        }

        return <EmptyHeart id={clickLikeId} 
            type="submit" onClick={clickLike}></EmptyHeart>
    }

    const clickDeletePost = () => {
        deletePost(id, nextUrlNum)
    }

    const getTagName = () => {
        let k = 0
        while (k < tagList.length) {
            if (tagList[k].id === tag) {
                return (<Tag className="tag">#{tagList[k].name}</Tag>)
            }
            k++
        }
    }

    const canDelete = () => {
        if (user === mainUserId) {
            return <X2 className="delete_post" id={deletePostId} type="submit" onClick={clickDeletePost}>X</X2>
        }
    }

    const profile_url = "/profile/" + user
    
    return (
        <Box className="container">
            {canDelete()}
            <TL>
                <CreateUser>
                    <CreateUserA href={profile_url} id="click_post_username">{username}</CreateUserA>
                </CreateUser>
                {getTagName()}
            </TL>
            <Hr></Hr>
            {toggleTitle()}
            <Body className="text" dangerouslySetInnerHTML={{__html: body}}>
            </Body>
            <Like className="like">
                {toggleHeart()}
                <span style={{"margin": "0 0 0 5px"}}>
                    {getLikeCount()}
                </span>
            </Like>
            <Hr></Hr>
            <Replies className="replies"> 
                {replies.map(reply => {
                    return (
                        <Reply key={reply.created} {...reply}
                            mainUserId={mainUserId}
                            num={nextRepliesNum}
                            deleteReply={(a,b,c) => deleteReply(a,b,c)}
                            addReplyLike={(a,b) => addReplyLike(a,b)}
                            deleteReplyLike={(a,b,c) => deleteReplyLike(a,b,c)}
                        />
                     )
                })}
                {load()}
            </Replies>
            <div className="add_reply">
                <TextArea2 id={replyTextId} placeholder="Write reply..." innerRef={node => {text = node;}} />
                <Button2 id={replyButtonId} type="submit" 
                    onClick={clickReply}>Reply</Button2>
            </div>
        </Box>
    )
}

export default Post
