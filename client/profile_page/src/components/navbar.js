import React from 'react'
import { UL, A, A2, LI, LI2, Logo } from './css'

const Navbar = ({ userId, mainUserId, logout }) => {
    const isMe = () => {
        if (userId !== mainUserId) {
            return <LI><A href='/profile/' id="profile_button">My Profile</A></LI>
        }
    }

    return (
        <UL className="navbar">
            <Logo><A2 href='/' id="main_button">Folivora</A2></Logo>
            {isMe()}
            <LI2><A id="logout_button" onClick={logout}>Logout</A>
            </LI2>
        </UL>
    )
}

export default Navbar
