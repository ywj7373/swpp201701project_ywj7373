import React from 'react'
import Follow from './follow'
import { FollowBox, BoxTitle } from './css'

const Followers = ({following, username, newProfile}) => {
    const renderFollow = (following) => {
        if (following.length > 0) {
            return following.map((follow) => {
                return (
                    <Follow key={follow.id} {...follow}
                        newProfile={(a) => newProfile(a)}
                    />
                )
            })
        }
    }
    
    return (
        <FollowBox>
           <BoxTitle>{username} Following List</BoxTitle>
           {renderFollow(following)}
        </FollowBox>
    )
}

export default Followers
