export const createNewInitialState = () => {
    var url = window.location.pathname
    var arr = url.split('/')
    let id = parseInt(arr[arr.length-1])

    return {
        mainUserId: 0,
        userId: id,
        username: '',
        image: '',
        nextUrl: '/api/user/' + id + '/post/',
        nextUrlNum: 0,
        loadingMore: false,
        fullList: false,
        lid: 0,
        posts: [],
        tagList: [],
        following: []
    }
}

const initialState = createNewInitialState()

export default initialState
