import { connect } from 'react-redux'
import Navbar from '../components/navbar'
import { logout } from '../actions/actions'

const mapStateToProps = (state) => {
    return {
        userId: state.userId,
        mainUserId: state.mainUserId
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => {
            dispatch(logout())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar)
