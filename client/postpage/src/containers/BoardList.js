import { connect } from 'react-redux'
import { BoardList } from '../components/BoardList'
import { addBoard, getPostRequest } from '../actions/post_action'

const mapStateToProps = ( state ) => {
  console.log('BoardList container state')
  return {
    boardliststate: state
  }
}

const mapDispatchToProps = (dispatch) => {
  console.log('BoardList container dispatch')
  return {
    getPostRequest : () => {
      dispatch(getPostRequest())
      console.log("getpostrequest")
    },
    addBoard : () => {
      dispatch(addBoard())
      console.log("addBoard--")
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BoardList)
