import { connect } from 'react-redux'
import { PostList }  from '../components/PostList'
import { postBoard } from '../actions/post_action'


const mapStateToProps = (state) => {
  console.log('postlist container state')
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  console.log('postlist container dispatch')
  return {
    PostBoard: (body) => {
      dispatch(postBoard(body))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostList)
