import { connect } from 'react-redux'
import { Board } from '../components/Board'
import { addReply } from '../actions/post_action'

const mapStateToProps = (state) => {
  console.log('Board container mapstate')
  return {
   replyliststate: state
  }
}

const mapDispatchToProps = (dispatch) => {

console.log('board container mapDispatch')
  return {
    onAddReply : (id, body) => {
      dispatch(addReply(id, body))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Board)
