import React, { PropTypes } from 'react'


var form_style = {
    "zIndex": "1",
    "position": "relative",
    "maxWidth": "45%",
    "margin": "0 auto 100px",
    "padding": "45px",
    "textAlign": "center",
    "boxShadow": "0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 5 rgba(0,0,0,0.24)",
    "background": "#FFFFFF"
}

var input_style = {
    "outline": "0",
    "background": "#f2f2f2",
    "width": "100%",
    "border": "0",
    "margin": "0 0 15px",
    "padding": "15px",
    "boxSizing": "border-box",
    "fontSize": "14px"
}

var button_style = {
    "fontFamily": "Monospace",
    "outline": "0",
    "background": "#4CAF50",
    "width": "50%",
    "border": "0",
    "padding": "15px",
    "color": "#FFFFFF",
    "fontSize": "14px",
    "transitionDuration": "0.4s",
    "cursor": "pointer"
}

export const PostList = ({ statefunction, PostBoard }) => {

  let textarea;
  console.log('PostList')

  // const onSubmit = () => {
  //   if(textarea !== undefined) {
  //     onAddBoard(textarea.value);
  //     textarea.value = '';
  //   }
  // };

  const onPost = () => {
    if(textarea !== undefined) {
      PostBoard(textarea.value);
      textarea.value = '';
    }
  };


  return (
    <div style={form_style}>
      <div>
        <textarea ref={node => {textarea = node;}} style={input_style} />
      </div>
      <div>
        <button style={button_style} type="submit" onClick={onPost}>Post Board</button>
      </div>
    </div>
  )
}

PostList.propTypes = {
  reverse: PropTypes.bool,
  children: PropTypes.node,
}
