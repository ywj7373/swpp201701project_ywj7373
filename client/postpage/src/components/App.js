import React, { Component, PropTypes } from 'react'
import PostList from '../containers/PostList'
import BoardList from '../containers/BoardList'
import './App.css'


var body_style = {
  "background": "lightgreen",
  "height": "100%",
  "margin": "0",
  "padding": "0"
}

var container_style = {
  "width": "auto",
  "padding": "15% 0 0",
  "margin": "auto"
}


class App extends Component {
render() {
  console.log('Appjs')
  return (
    <div className="App" style={body_style} className="body">
      <div style={container_style} className="container">
        <PostList/ >
        <BoardList/ >
      </div>
    </div>
  )
}
}

export default App
