import React, {PropTypes} from 'react'
import Reply from '../containers/Reply'
import $ from 'jquery'

let weekday = new Array();
weekday[0]="Sun";
weekday[1]="Mon";
weekday[2]="Tue";
weekday[3]="Wed";
weekday[4]="Thu";
weekday[5]="Fri";
weekday[6]="Sat";

const i =''

var board_style = {
    "outline": "0",
    "background": "#f2f2f2",
    "width": "100%",
    "border": "0",
    "margin": "10px 0 10px",
    "padding": "15px",
    "boxSizing": "border-box",
    "fontSize": "14px"
}

// const RenderR = (data) => (
//   <div> {data} </div>
// )
//
// const RenderRs = (pid) => {
//   let Replies = []
//   const request = $.get("http://swpp.devluci.com:11177/api/post/"+pid+"/reply/")
//   request.done(function(data) {
//     if(data.results !== undefined) {
//     for (let i=0; i<1, data.results[i] !== undefined; i++) {
//       Replies.push(<Reply key={data.results[i].id} id={data.results[i].id} text={data.results[i].body} created={data.results[i].created} post={data.results[i].post} />)
//
//     }
//     console.log(Replies[0])
//
//     }
//     console.log(<Reply id={1}/>)
//   })
//   return RenderR(Replies)
// }

export const Board = ({ replyliststate, created, pid, body, replies, onAddReply}) => {
  let textarea;

  const onSubmit = () => {
    if(textarea !== undefined) {
      onAddReply(pid, textarea.value);
      textarea.value = '';
    }
  };

  // const request = $.get(post_url)
  //
  // request.done(function(data) {
  //   console.log(data)
  //
  //   for (let i=0; i<10; i++) {
  //     let board = {
  //       pid: data.results[i].id,
  //       body: data.results[i].body,
  //       created: data.results[i].created,
  //     }
  //
  //     boardliststate.push(board)
  //     console.log(board)
  //   }
  // })

  // const request = $.get("http://swpp.devluci.com:11177/api/post/"+pid+"/reply/")
  //   request.done(function(data) {
  //     if(data.results[0] !== undefined) {
  //       // console.log(data.results[0].body)
  //        i = data.results[0].body}
  //
  //   })

///////////////////////////////////////


  // {boardliststate.map(board =>
  //   <Board key={board.id}
  //     {...board} />
  // )}

  // {boardliststate.map((board) => {
  //   return (<Board key={board.id} id={board.id} created={board.created} body={board.body}/>)
  //   }
  // )}

if(replies !== undefined) {
  return (

  <div style={board_style}>
    <div>{created.toString()}</div>
    <div>
      {pid}
      {body}
    </div>
    <div>
      <textarea ref={node => {textarea = node;}} />
    </div>
    <div>
      <button type="submit" onClick={onSubmit}>Reply</button>
    </div>
    <div>
    {replies.map(reply =>
      <Reply  id={reply.id} text={reply.body} />
    )}

    </div>
  </div>
  )
  }
  else {
    return (

    <div style={board_style}>
      <div>{created.toString()}</div>
      <div>
        {pid}
        {body}
      </div>
      <div>
        <textarea ref={node => {textarea = node;}} />
      </div>
      <div>
        <button type="submit" onClick={onSubmit}>Reply</button>
      </div>
      <div>
      </div>
    </div>
    )
  }

}


// <Reply text={'d'} />
// {console.log(pid)}
// {console.log(i)}
// {console.log('reply')}
// <Reply text={i.toString()} />

// {replies.map((reply) => {
//   console.log(reply)
//   return (
//   <Reply key={reply.id} id={reply.id} text={reply.text} created={reply.created} post={reply.post}/>)
// }
// )}

// {created.getDate().toString()} / {(created.getMonth() + 1).toString()} {weekday[created.getDay()]}

Board.propTypes = {
  //onAddReply: PropTypes.func.isRequired,
  replyliststate: PropTypes.arrayOf(PropTypes.shape({
   id: PropTypes.number,
   body: PropTypes.string.isRequired})),
  pid: PropTypes.number,
  created: PropTypes.instanceOf(Date).isRequired,
  body: PropTypes.string,


  reverse: PropTypes.bool,

}

export default Board
