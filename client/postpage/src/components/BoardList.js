import React, { PropTypes } from 'react'
import Board from '../containers/Board'
import Reply from './Reply'

var form_style = {
    "zIndex": "1",
    "position": "relative",
    "maxWidth": "45%",
    "margin": "0 auto 100px",
    "padding": "45px",
   // "textAlign": "center",
    "boxShadow": "0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 5 rgba(0,0,0,0.24)",
    "background": "#FFFFFF"
}

export const BoardList = ({ boardliststate  , getPostRequest, addBoard}) => {

  console.log(boardliststate)

  return (
    <div style={form_style}>

    {boardliststate.map(board =>
      <Board key={board.pid}
        {...board} />
    )}

    </div>
  );
};
//
// {boardliststate.map(board =>
//   <Board key={board.id}
//     {...board} />
// )}

// {boardliststate.map((board) => {
//   return (<Board key={board.id} id={board.id} created={board.created} body={board.body}/>)
//   }
// )}
BoardList.propTypes = {

  boardliststate: PropTypes.arrayOf(PropTypes.shape({
    created: PropTypes.instanceOf(Date).isRequired,
    pid: PropTypes.number,
    body: PropTypes.string,
    replies: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      body: PropTypes.string,
      post: PropTypes.number
    }))})),
  reverse: PropTypes.bool
}

export default BoardList
