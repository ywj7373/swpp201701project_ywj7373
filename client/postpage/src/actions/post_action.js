import $ from 'jquery'

const post_url = "http://swpp.devluci.com:11177/api/post/"

var post_id = 0

export function postBoard(body) {

  var token = document.getElementById("csrftoken").value;
  const request = $.post(post_url, {

       body: body,
       csrfmiddlewaretoken: token

  });

  return dispatch => {
    request.done(response => {
		dispatch(getPostRequest())
	})
  }
}

export function getPostRequest() {
  const request = $.get(post_url)

  return dispatch => {
	   request.done(function(data) {
        dispatch(addBoard(data.results[0].id, data.results[0].body, data.results[0].created))
	   })
  }
}


var board_id = 0
export const addBoard = (pid, body, created) => {
  return {
    type: 'ADD_BOARD',
    created,
    body,
    pid
  }
}

var nextReplyId = 0
export const addReply = (pid, body) => {
  var token = document.getElementById("csrftoken").value;
console.log("add")
  const request = $.post("http://swpp.devluci.com:11177/api/post/"+pid+"/reply/", {

       body: body,
       post: pid,
       csrfmiddlewaretoken: token

  });
  return {
    type: 'ADD_REPLY',
  //   id: nextReplyId++,
  //   pid,
  //   body
  }

}

export const POST_BOARD_REQUEST = 'POST_BOARD_REQUEST'

export const postBoardRequest = ( body ) => {
  return {
    type: POST_BOARD_REQUEST,
    body
  }
}
