import React from 'react';
import { render } from 'react-dom'
import {createStore, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import App from './components/App'
//import reducer from './reducers/index'
//import boardReducer from './store/board/reducer'
//import replyReducer from './store/reply/reducer'
import boardReducer from './reducers/post_reducer'
import { initialState } from './reducers/selectors'
// import { getPostRequest } from './actions/post_action'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  boardReducer,
  composeEnhancers(applyMiddleware(thunk))
)


render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('root')
)
