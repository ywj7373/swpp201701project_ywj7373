export const createNewInitialState = () => {
    return {
        mainUserId: 0,
        userId: 0,
        username: '',
        image: '',
        nextUrl: '/api/user/post/',
        nextUrlNum: 0,
        loadingMore: false,
        fullList: false,
        lid: 0,
        posts: [],
        tagList: [],
        following: []
    }
}

const initialState = createNewInitialState()

export default initialState
