import { connect } from 'react-redux'
import Followers from '../components/followers'

const mapStateToProps = (state) => {
    return {
        following: state.following,
        username: state.username
    }
}

export default connect(mapStateToProps, null)(Followers)
