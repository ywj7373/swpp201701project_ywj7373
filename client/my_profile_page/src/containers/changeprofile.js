import { connect } from 'react-redux'
import ChangeProfile from '../components/changeprofile'
import { addImage } from '../actions/actions'

const mapStateToProps = (state) => {
    return {
        userId: state.userId,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addImage: (file) => {
            dispatch(addImage(file))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangeProfile)
