import React from 'react'
import { User, Info, Image, Follow, Username, AA } from './css'

const UserInfo = ({ userId, following, username, image, gotoProfile }) => {
    const getFollowingCount = () => {
        let m = following
        if (m != undefined) {
            return m.length
        }
        else {
            return 0
        }
    }

    const getImage = () => {
        if (image === "") {
            return <Image className="empty_image" src="https://museum.wales/media/40374/thumb_480/empty-profile-grey.jpg" alt="User Image" />
        }
        else {
            return <Image className="image" src={image} alt="User Image" />
        }
    }

    return (
        <User className="UserInfo">
            {getImage()}       
            <Info>
                <Username>
                    <AA href='/profile' id="click_username">{username}</AA>
                </Username>
                <Follow className="Following">
                    Following {getFollowingCount()}
                </Follow>
            </Info>
        </User>
    )
}

export default UserInfo
