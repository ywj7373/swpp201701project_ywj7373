# A. Bahavior list
* 로그인 (Back-End) - 박지환
* 로그아웃 (Back-End) - 박지환
* 회원가입 (Back-End) - 박지환
* 아직 작업에 익숙하지 않고, 시간이 부족하여 프론트엔드 개발을 끝내지 못한 상태

# B. Server API
## 1. 개요 
Server: http://45.76.105.190:8000

목차

* 로그인/회원가입

## 2. 로그인/회원가입
HTTP status code

* 200: Success
* 406: 유효하지 않은 데이터

Response format

* message: 오류 메세지 (생략될 수 있음)

### 2.1. 로그인
/login

Method: POST

Data

* login_username: 아이디
* login_password: 비밀번호

### 2.2. 로그아웃
/logout

Method: POST

Data

* 없음

### 2.3. 회원가입
/join

Method: POST

Data

* login_username: 아이디
* login_password: 비밀번호
* join_email: 이메일
* join_family_name: 성
* join_given_name: 이름

Format Constraint

* login_username: 영어 대소문자, 숫자, 언더스코어(_)로 이뤄진 7-12 글자, admin과 swpp는 포함 불가능
* login_password: 9-32 글자
* join_email: '+' 문자가 포함되지 않은 올바른 이메일
* join_family_name: 영어 대소문자, 숫자, 한글로 최대 16 글자
* join_given_name: 영어 대소문자, 숫자, 한글로 최대 16 글자

DB Constraint

* login_username: Unique
* join_email: Unique

# C. Test code
* 아직 작업에 익숙하지 않고, 시간이 부족하여 개발을 끝내지 못한 상태

# D. 링크
* http://45.76.105.190:8000