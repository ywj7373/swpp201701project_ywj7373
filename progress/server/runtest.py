import json
import requests
from time import sleep
from random import randint

def get_json_or_error(link, auth):
    sleep(0.05)
    try:
        res = requests.get(link, auth=auth).json()
        return res
    except Exception:
        print("ERROR: Cannot get {0}".format(link))
        exit(1)

def check_key(json, key):
    if key not in json:
        print("{0} not in {1}".format(key, json))
        exit(1)

def join(start_num):
    for i in range(start_num, userN + start_num):
        login_username = "testUser{0}".format(i)
        login_password = "testUser{0}passwd".format(i)
        join_email = "testUser{0}@example.com".format(i)
        join_family_name = "testUserFname".format(i)
        join_given_name = "testUserGname".format(i)

        user_info = {
            'login_username': login_username,
            'login_password': login_password,
            'join_email': join_email,
            'join_family_name': join_family_name,
            'join_given_name': join_given_name
        }

        try:
            res = requests.post(link_join, data=user_info)
           
            if res.status_code != 200:
                res_dic = json.loads(res.json())
                res_message = res_dic['message']
                if res_message == "이미 사용 중인 아이디에요":
                    join(start_num+10)
                    break
                print("ERROR: Should not be allowed to join by {0}".format(link_join))
                exit(1)
        except Exception:
            print("ERROR: Cannot join by {0}".format(link_join))
            exit(1)

        users.append(user_info)


link = "http://swpp.devluci.com:8000/api/"

#1. Join the app with user test_user1-10
users = []
userN = 10
link_join = link + "join/"
print("1. Checking join")
join(1)


#2. Log in to the app with user test_user1-10
login_set = []
link_login = link + "login/"   
print("2. Checking login")
for user in users:
    login_info = {
        'login_username': user["login_username"],
        'login_password': user["login_password"],
    }
    login_set.append(login_info)

    try:
        res = requests.post(link_login, data=login_info)
        if res.status_code != 200:
            print("ERROR: Should not be allowed to log in by {0}".format(link_login))
            exit(1)   
    except Exception:
        print("ERROR: Cannot login by {0}".format(link_login))
        exit(1)

#3. Log out of the app by test_user10 who logged in last
link_logout = link + "logout/"
print("3. Checking logout")
try:
    res = requests.post(link_logout)
    if res.status_code != 200:
        print("ERROR: Should not be allowed to log out by {0}".format(link_logout))
        exit(1)   
except Exception:
    print("ERROR: Cannot logout by {0}".format(link_logout))
    exit(1)

#4. Create posts
link_post = link + "post/"
posts = []
postN = 20
print("4. Checking writing by POST /post")
for i in range(0, postN): 
    login_info = login_set[int(i/2)]

    auth = (login_info["login_username"], login_info["login_password"])
    body = {
        'body': "test_post{0}".format(i),
        'title': "test_title{0}".format(i),
    }
    posts.append(body)

    try:
        requests.post(link_post, data=body, auth=auth)
    except Exception:
        print("ERROR: Cannot write by Post {0}".format(link_post))
        exit(1)


#Set login info to 
login_info = login_set[0]
auth = (login_info["login_username"], login_info["login_password"]) 

#5. Get posts
posts_json = get_json_or_error(link_post, auth)
posts_json_results = []
print("5. Checking get post by GET /post")
while True:
    posts_json_results.extend(posts_json["results"])
    if posts_json["next"] == None:
        break
    else:
        posts_json = get_json_or_error(posts_json["next"], auth)

for post in posts:
    found = False
    for posts_json_result in posts_json_results:
        check_key(posts_json_result, "id")
        check_key(posts_json_result, "user")
        check_key(posts_json_result, "created")
        check_key(posts_json_result, "body")
        if posts_json_result["body"] == post["body"]:
            found = True
            break
    if not found:
        print("ERROR: Not found : {0}".format(post["body"]))
        exit(1)


#6. Delete posts
posts_to_delete = []
login_info_to_delete = login_set[userN - 1]
auth_to_delete = (login_info_to_delete["login_username"], login_info_to_delete["login_password"]) 
print("6. Checking DELETE /post/<post_id>")
for posts_json_result in posts_json_results:
    if posts_json_result["user"] == login_info_to_delete['login_username']:
        posts_to_delete.extend(posts_json_reulst["id"])

for post_to_delete in posts_to_delete:
    link_post_with_id = link_post + post_to_delete + "/"
    try:
        requests.delete(link_post_with_id, auth=auth_to_delete)
    except Exception:
        print("ERROR: Cannot DELETE {0}".format(link_post_with_id))
        exit(1)


#7. Create, get replies
link_reply = link_post + "<post_id>/reply/"
print("7. Checking reply by POST, GET /<post_id>/reply")
for i in range(0, 2):
    replies = []
    replyN = 20
    post_json_result = posts_json_results[i]
    id = post_json_result["id"]
    link_reply_with_id = link_post + str(id) + "/reply/"

    for j in range(0, replyN): 
        login_info = login_set[int(j/2)]
        auth = (login_info["login_username"], login_info["login_password"])
        body = {'body': "test_reply{0}".format(j), 'post': id}
        replies.append(body)

        try:
            requests.post(link_reply_with_id, data=body, auth=auth)
        except Exception:
            print("ERROR: Cannot reply by Post {0}".format(link_reply))
            exit(1)

    #Get replies
    replies_json = get_json_or_error(link_reply_with_id, auth)
    replies_json_results = []
    while True:
        replies_json_results.extend(replies_json["results"])
        if replies_json["next"] == None:
            break
        else:
            replies_json = get_json_or_error(replies_json["next"], auth)

    for reply in replies:
        found = False
        for replies_json_result in replies_json_results:
            check_key(replies_json_result, "id")
            check_key(replies_json_result, "user")
            check_key(replies_json_result, "post")
            check_key(replies_json_result, "created")
            check_key(replies_json_result, "body")
            if (replies_json_result["body"] == reply["body"] and
                    replies_json_result["post"] == id):
                found = True
                break
        if not found:
            print("ERROR: Not found : {0}".format(reply["body"]))
            exit(1)


#8. Delete replies
replies_to_delete = []
print("8. Checking DELETE /<post_id>/reply/<reply_id>")
post_json_result = posts_json_results[1]
id = post_json_result["id"]
link_reply_with_id = link_post + str(id) + "/reply/"

replies_json = get_json_or_error(link_reply_with_id, auth)
replies_json_results = []
while True:
    replies_json_results.extend(replies_json["results"])
    if replies_json["next"] == None:
        break
    else:
        replies_json = get_json_or_error(replies_json["next"], auth)

for replies_json_result in replies_json_results:
    if replies_json_result["user"] == login_info_to_delete['login_username']:
        replies_to_delete.extend(replies_json_reulst["id"])

for reply_to_delete in replies_to_delete:
    link_reply_to_delete = link_reply_with_id + reply_to_delete + "/"
    try:
        requests.delete(link_reply_to_delete, auth=auth_to_delete)
    except Exception:
        print("ERROR: Cannot DELETE {0}".format(link_reply_to_delete))
        exit(1)


#9. Create, get likes
link_like = link_post + "<post_id>/like/"
print("9. Checking like by POST, GET /post/<post_id>/like")
for i in range(0, 2):
    post_json_result = posts_json_results[i]
    id = post_json_result["id"]
    link_like_with_id = link_post + str(id) + "/like/"

    for j in range(0, userN): 
        login_info = login_set[j]
        auth = (login_info["login_username"], login_info["login_password"])

        try:
            requests.post(link_like_with_id, auth=auth)
        except Exception:
            print("ERROR: Cannot like by Post {0}".format(link_like))
            exit(1)

        #Get likes
        likes_json = get_json_or_error(link_like_with_id, auth)
        like_json = likes_json[0]

        check_key(like_json, "id")
        check_key(like_json, "user")
        check_key(like_json, "post")
        check_key(like_json, "created")

        if (like_json["post"] != id):
            print("ERROR: Not found : {0}".format("like"))
            exit(1)


#10. Checking like only once
print("10. Checking double likes is prohibited")
id = post_json_result["id"]
link_like_with_id = link_post + str(id) + "/like/"

login_info = login_set[0]
auth = (login_info["login_username"], login_info["login_password"])

try:
    requests.post(link_like_with_id, auth=auth)
except Exception:
    print("ERROR: Cannot like by Post {0}".format(link_like))
    exit(1)

likes_json = get_json_or_error(link_like_with_id, auth)

if (len(likes_json) != userN):
    print("ERROR: Double likes isn't prohibited")


#11. Delete replies
likes_to_delete = []
print("11. Checking DELETE /post/<post_id>/like/<like_id>")
post_json_result = posts_json_results[1]
id = post_json_result["id"]
link_like_with_id = link_post + str(id) + "/like/"

likes_json = get_json_or_error(link_like_with_id, auth_to_delete)
like_json = likes_json[0]

link_like_to_delete = link_like_with_id + str(like_json["id"]) + "/"
try:
    requests.delete(link_like_to_delete, auth=auth_to_delete)
except Exception:
    print("ERROR: Cannot DELETE {0}".format(link_like_to_delete))
    exit(1)


#12. Get info of logged in user
link_user = link + "user/"
print("12. Checking GET logged in user info by /user")
users_json = get_json_or_error(link_user, auth)
user_json = users_json[0]
if user_json["username"] != login_info["login_username"]:
    print("ERROR: Cannot GET logged in user info by {0}".format(link_user))
    exit(1)


#13. Get user info
print("13. Checking GET user info by /user/<user_id>")
post_json_result = posts_json_results[0]
user_id = post_json_result["user"]
link_user_with_id = link_user + str(user_id) + "/"
user_json = get_json_or_error(link_user_with_id, auth)
if user_json["id"] != user_id:
    print("ERROR: Cannot GET user info by {0}".format(link_user_with_id))
    exit(1)


#14. Get user post
print("14. Checking GET user's post by /user/<user_id>/post")
link_user_post = link_user_with_id + "post/"
posts_json = get_json_or_error(link_user_post, auth)
posts_json_results = []
while True:
    posts_json_results.extend(posts_json["results"])
    if posts_json["next"] == None:
        break
    else:
        posts_json = get_json_or_error(posts_json["next"], auth)

for posts_json_result in posts_json_results:
    if posts_json_result["user"] != user_id:
        print("ERROR: Cannot GET user's post by {0}".format(link_user_post))
        exit(1)


#15. post, get follow
link_follow = link + "follow/"
print("15. Checking follow by POST, GET /follow")
data = {'follow': user_id,}
try:
    requests.post(link_follow, data=data, auth=auth)
except Exception:
    print("ERROR: Cannot follow by Post {0}".format(link_follow))
    exit(1)

follows_json = get_json_or_error(link_follow, auth)
follow_len = len(follows_json)
follow_json = follows_json[0]
if follow_json["follow"] != user_id:
    print("ERROR: Cannot GET follow by {0}".format(link_follow))
    exit(1)


#16. Checking follow only once
print("16. Checking double follows is prohibited")
try:
    requests.post(link_follow, data=data, auth=auth)
except Exception:
    print("ERROR: Cannot follow by Post {0}".format(link_follow))
    exit(1)

follows_json = get_json_or_error(link_follow, auth)
if len(follows_json) != follow_len:
    print("ERROR: Double follows isn't prohibited")
    exit(1)


#17. deleting follow
print("17. Checking follow by DELETE /follow/<follow_id>")
link_follow_with_id = link_follow + str(user_id) + "/"
try:
    requests.delete(link_follow_with_id, auth=auth)
except Exception:
    print("ERROR: Cannot delete follow by DELETE {0}".format(link_follow_with_id))
    exit(1)

#18. Create tags
link_tag = link + "tag/"
link_tag_add = link_tag + "add/"
tags = []
tagN = 4
print("18. Checking add tags by POST /tag")
auth = ("swpp201710", "good!morning123")
for i in range(0, tagN): 
    data = {
        'name': "test_tag{0}".format(i),
    }
    tags.append(data)

    try:
        requests.post(link_tag_add, data=data, auth=auth)
    except Exception:
        print("ERROR: Cannot add tag by Post {0}".format(link_tag))
        exit(1)


#19. Get tags
tags_json = get_json_or_error(link_tag, auth)
print("19. Checking get tags by GET /tag")
for tag in tags:
    found = False
    for tag_json in tags_json:
        check_key(tag_json, "id")
        check_key(tag_json, "name")
        if tag_json["name"] == tag["name"]:
            found = True
            break
    if not found:
        print("ERROR: Not found : {0}".format(tag["name"]))
        exit(1)


print("TEST SUCCESSFUL")
