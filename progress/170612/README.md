# A. 금주 작업한 Bahavior list
* 유저의 follow 정보 가져오기
    * Back-End(박지환)
        * /api/user/<user_id>/follow : 해당 id 유저가 팔로우 하고 있는 유저의 리스트 가져오기
* 팔로우 할 유저 추천
    * Back-End(박지환)
        * /api/follow/rec
* 댓글 좋아요
    * Back-End(박지환)
        * /api/reply/<reply_id>/like : 해당 id 댓글에 좋아요 하기
        * /api/reply/<reply_id>/like/<like_id> : 해당 reply id의 댓글에서 해당하는 like id의 좋아요를 가져오기
* 태그
    * Front-End(전영웅)
        * 포스팅 시 태그 추가 기능
        * 작성된 포스트에 선택한 태그 표시 기능
* 댓글
    * Front-End(전영웅) - 댓글 삭제 기능
* 프로필 페이지
    * Front-End(전영웅)
         * 자신이 follow하고 있는 유저 리스트 표시
         * 타인의 프로필 페이지 표시
* Jest 테스트 코드(박희준)



# B. Back-End
## 1. API Spec
* https://bitbucket.org/Gamhat/swpp201701project/wiki/Home

## 2. Test code
* progress/server/runtest.py
* 실행법
    * $ python3 runtest.py
* 테스트 항목
    * 기존 항목
    
    1. Checking join
    1. Checking login
    1. Checking logout
    1. Checking writing by POST /post
    1. Checking get post by GET /post
    1. Checking reply by POST, GET /post/<post id>/reply 
    1. Checking DELETE by /post/<post id>
    1. Checking DELETE by /post/<post id>/reply/<reply id>
    1. Checking like by POST, GET /post/<post id>/like
    1. Checking double likes is prohibited
    1. Checking DELETE /post/<post id>/like/<like id>
    1. Checking GET user info by /user/<user id>
    1. Checking follow by POST, GET /follow
    1. Checking double follows is prohibited
    1. Checking follow by DELETE /follow/<follow id>
    1. Checking adding tags by POST /tag
    1. Checking get tags by GET /tag    
    1. Checking GET logged in user info by /user
    1. Checking GET user's post by /user/<user_id>/post

    * 마지막에 “TEST SUCCESSFUL”이 출력될 경우 성공

# C. Front-End
## 1. Test code
* progress/client/login_test.py
    * 로그인, 조인 페이지 테스트
* progress/client/main_test.py
    * 메인 페이지 테스트
* 실행법
    * $ python3 login_test.py
    * $ python3 main_test.py
    * 순서 무관. 하나만 돌려도 상관 없음
* 테스트 항목(login_test)
    1. Check join : 가입 시 username 길이, email 형식 등 제약조건 테스트
        1. check username fail
        1. check password fail
        1. check email fail
        1. check family name fail
        1. check given name fail
        1. check join success
    1. Check login
        1. check login success
        1. check login fail
    1. Check same info : 기존 유저와 동일한 정보로 가입시 에러 발생하는 지 테스트
        1. check same username error
        1. check same email error

    * 마지막에 “Login, Join page test complete”이 출력될 경우 성공

* 테스트 항목(main_test)

    1. post test - 포스트 작성 테스트
    1. reply test - 댓글 작성 테스트
    1. reply load more test - 댓글의 더보기 버튼(load more) 테스트
    1. like test - 좋아요 테스트
    1. cancel like test - 좋아요 취소 테스트
    1. logout test - 로그아웃 테스트
    1. profile test - 프로필 페이지 동작 테스트
    1. upload profile image test - 프로필 이미지 변경 테스트

    * 마지막에 “Main page test complete”이 출력될 경우 성공

# D. Redux unit test
## 1. Test result
* Pipelines
    * Pipelines 페이지를 통해 main_page, login_page의 test result 확인 가능
* Main page
    * ![Alt text](http://i.imgur.com/NxCjlug.png)
* Login page
    * ![Alt text](http://i.imgur.com/BcTKK9i.png)

# E. Web service 링크
* http://swpp.devluci.com:8000/