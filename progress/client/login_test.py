import time
import string
import random
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
from swpp_module import *

def join_test(browser, element, join_button, eid):
    if eid == "family_name" or eid == "given_name":
        element.send_keys(str_generator(20))
    else:
        element.send_keys(str_generator(3))
    time.sleep(1)
    join_button.click()
    time.sleep(2)
    error = browser.find_element_by_id('error_message')
    
    if eid == "join_username" and "아이디는 영어 대소문자, 숫자, 언더스코어(_)로 이뤄진 7-12 글자만 사용 가능해요" != error.text:
        print("no username error shown")
        browser.quit()
        exit(1)
    elif eid == 'join_password' and "비밀번호는 9-32 글자만 가능해요" != error.text:
        print("no password error shown")
        browser.quit()
        exit(1)
    elif eid == 'join_email' and "잘못된 이메일 형식이에요" != error.text:
        print("no email error shown")
        browser.quit()
        exit(1)
    elif eid == 'family_name' and "성은 영어 대소문자, 한글로 최대 16 글자까지만 가능해요" != error.text:
        print("no family name error shown")
        
        exit(1)
    elif eid == 'given_name' and "이름은 영어 대소문자, 한글로 최대 16 글자까지만 가능해요" != error.text:
        print("no given name error shown")
        browser.quit()
        exit(1)

def test_change_page(browser, eid):
    try:
        browser.find_element_by_id(eid)
    except NoSuchElementException:
        print ("wrong transition")
        browser.quit()
        exit(1)

def test_login_error(element, fail):
    style = element.get_attribute('style')
    if fail == True and style == "display: none;":
        print ("login error did not show up")
        browser.quit()
        exit(1)
    elif fail == False and style != "display: none;":
        print ("login error showed up when login was successful")
        browser.quit()
        exit(1)

def test_same_text_error(browser, eid):
    error = browser.find_element_by_id('error_message')
    if eid == 'same_name' and error.text != "이미 사용 중인 아이디에요":
        print ("same username error did not show up")
        exit(1)
    elif eid =='same_email' and error.text != "이미 사용 중인 이메일 주소에요":
        print ("same email error did not show up")
        browser.quit()
        exit(1)

#setup
browser = webdriver.Chrome('/usr/local/bin/chromedriver')
browser.get('http://swpp.devluci.com:8000/')

time.sleep(1)
#click join
join = browser.find_element_by_id('change_join_button')
join.click()
time.sleep(1)
    
#label elements

join_button = browser.find_element_by_id('join_button')
join_name = browser.find_element_by_id('join_username')
join_pwd = browser.find_element_by_id('join_password')
join_email = browser.find_element_by_id('join_email')
join_fname = browser.find_element_by_id('family_name_field')
join_gname = browser.find_element_by_id('given_name_field')
    
#check join fail
#check username faili
join_test(browser, join_name, join_button, 'join_username')
join_name.clear()
time.sleep(1)
name = str_generator(8)
join_name.send_keys(name)
time.sleep(2)
#check password fail
join_test(browser, join_pwd, join_button, 'join_password')
join_pwd.clear()
time.sleep(1)
join_pwd.send_keys(str_generator(10))
time.sleep(2)
#check email fail
join_test(browser, join_email, join_button, 'join_email')
join_email.clear()
time.sleep(1)
email = str_generator(8)+"@example.com"
join_email.send_keys(email)
time.sleep(2)
#check family name fail
join_test(browser, join_fname, join_button, 'family_name')
join_fname.clear()
time.sleep(1)
join_fname.send_keys(str_generator(5))
time.sleep(2)
#check given name fail
join_test(browser, join_gname, join_button, 'given_name')
join_gname.clear()
time.sleep(1)
join_gname.send_keys(str_generator(5))
time.sleep(1)
#check join success
join_button.click()
time.sleep(2)

#label login elements
login_button = browser.find_element_by_id('login_button')
login_name = browser.find_element_by_id('login_username')
login_pwd = browser.find_element_by_id('login_password')
#check login success
time.sleep(1)
#check login fail
time.sleep(1)
login_name.clear()
time.sleep(1)
login_pwd.clear()
time.sleep(1)
login_button.click()
time.sleep(2)
login_error = browser.find_element_by_id('error_message_2')
test_login_error(login_error, True)

#check same username error
join = browser.find_element_by_id("change_join_button")
join.click()
time.sleep(2)

join_button = browser.find_element_by_id('join_button')
join_name = browser.find_element_by_id('join_username')
join_pwd = browser.find_element_by_id('join_password')
join_email = browser.find_element_by_id('join_email')
join_fname = browser.find_element_by_id('family_name_field')
join_gname = browser.find_element_by_id('given_name_field')

join_name.clear()
join_pwd.clear()
join_name.send_keys(name)
time.sleep(1)
join_pwd.send_keys(str_generator(10))
time.sleep(1)
join_email.send_keys(str_generator(5)+"@example.com")
time.sleep(1)
join_fname.send_keys(str_generator(5))
time.sleep(1)
join_gname.send_keys(str_generator(5))
time.sleep(1)
join_button.click()
time.sleep(3)
test_same_text_error(browser, 'same_name')
time.sleep(1)

#check same email error
join_name.clear()
time.sleep(1)
join_name.send_keys(str_generator(10))
time.sleep(1)
join_email.clear()
time.sleep(1)
join_email.send_keys(email)
time.sleep(1)
join_button.click()
time.sleep(2)
test_same_text_error(browser, 'same_email')

#check complete, exit
time.sleep(1)
browser.quit()
print ("Login, Join page test complete")
