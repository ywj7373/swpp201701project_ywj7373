import string
import random

def str_generator(size):
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(size))
