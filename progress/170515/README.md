# A. 금주 작업한 Bahavior list
* 로그인 (/login)
    * Front-End - 전영웅
    * 로그인 된 상태일 경우 바로 메인페이지로 전환 등의 세부 기능 추가
* 포스트 (/post)
    * Back-End - 박지환. DELETE 추가
    * Front-End - 박희준
* 댓글달기 (/post/<post id>/reply)
    * Back-End - 박지환. DELETE 추가
    * Front-End - 박희준
* 좋아요 (/post/<post id>/like)
    * Back-End - 박지환
* Back-End 테스트 코드 - 현유지
* Front-End 테스트 코드 - 현유지

# B. Back-End
## 1. API Spec
* 포스트(/post) DELETE 추가
* 댓글(/post/<post id>/reply) DELETE 추가, url 형식 수정
* 좋아요(/post/<post_id>/like) 추가
    * GET, POST, DELETE
* 상세 양식은 https://bitbucket.org/Gamhat/swpp201701project/wiki/Server%20API

## 2. Test code
* progress/server/runtest.py
* 실행법
    * http://swpp.devluci.com:8000/api/ 로 접속(테스트용 서버)
    * $ python3 runtest.py
* 테스트 항목
    * 기존 항목
    
    1. Checking join
    1. Checking login
    1. Checking logout
    1. Checking writing by POST /post
    1. Checking GET by /post
    1. Checking reply by POST, GET /post/<post id>/reply 
    
    * 추가된 항목
    
    1. Checking DELETE by /post/<post id>
    1. Checking DELETE by /post/<post id>/reply/<reply id>
    1. Checking like by POST, GET /post/<post id>/like
    1. Checking double likes is prohibited
    1. Checking DELETE /post/<post id>/like/<like id>

    * 마지막에 “TEST SUCCESSFUL”이 출력될 경우 성공

# C. Front-End
## 1. Test code
* progress/client/login_page/login_test.py
* Selenium 활용
* 실행법
    * $ python3 login_test.py
* 테스트 항목
    1. Check join : 가입 시 username 길이, email 형식 등 제약조건 테스트
        1. check username fail
        1. check password fail
        1. check email fail
        1. check family name fail
        1. check given name fail
        1. check join success
    1. Check login
        1. check login success
        1. check login fail
    1. Check same info : 기존 유저와 동일한 정보로 가입시 에러 발생하는 지 테스트
        1. check same username error
        1. check same email error

    * 마지막에 “test complete”이 출력될 경우 성공

# D. Web service 링크
* http://swpp.devluci.com:8000/