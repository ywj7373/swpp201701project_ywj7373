# A. 금주 작업한 Bahavior list
* 유저 정보 가져오기
    * Back-End(박지환)
        * /api/user : 현재 로그인 된 유저의 정보 가져오기
        * /api/user/<user_id> : 해당 id의 유저의 정보 가져오기
        * /api/user/<user_id>/post : 해당 id의 유저가 작성한 포스트 가져오기
* 포스트, 댓글
    * Front-End(전영웅) - 작성자 정보(username), 작성 시간 추가, 댓글 삭제 추가
    * Back-End(박지환) - 작성자 정보(username) 추가
* 봇
    * Back-End(박지환) - 5분 마다 글을 작성하는 봇 추가
* 프로필 이미지
    * Front-End(전영웅) - 메인 화면 username 옆에 프로필 이미지 추가
* 프로필 페이지
    * Front-End(전영웅) - 자신이 작성한 글 확인, 자신의 프로필 이미지 변경이 가능한 프로필 페이지 추가
* Jest 테스트 코드(박희준)
* Back-End 테스트 코드(현유지)
* Front-End 테스트 코드(현유지)



# B. Back-End
## 1. API Spec
* https://bitbucket.org/Gamhat/swpp201701project/wiki/Home

## 2. Test code
* progress/server/runtest.py
* 실행법
    * $ python3 runtest.py
* 테스트 항목
    * 기존 항목
    
    1. Checking join
    1. Checking login
    1. Checking logout
    1. Checking writing by POST /post
    1. Checking get post by GET /post
    1. Checking reply by POST, GET /post/<post id>/reply 
    1. Checking DELETE by /post/<post id>
    1. Checking DELETE by /post/<post id>/reply/<reply id>
    1. Checking like by POST, GET /post/<post id>/like
    1. Checking double likes is prohibited
    1. Checking DELETE /post/<post id>/like/<like id>
    1. Checking GET user info by /user/<user id>
    1. Checking follow by POST, GET /follow
    1. Checking double follows is prohibited
    1. Checking follow by DELETE /follow/<follow id>
    1. Checking adding tags by POST /tag
    1. Checking get tags by GET /tag
    
    * __추가된 항목__
    
    1. Checking GET logged in user info by /user
    1. Checking GET user's post by /user/<user_id>/post

    * 마지막에 “TEST SUCCESSFUL”이 출력될 경우 성공

# C. Front-End
## 1. Test code
* progress/client/login_test.py
    * 로그인, 조인 페이지 테스트
* progress/client/main_test.py
    * 메인 페이지 테스트
* 실행법
    * $ python3 login_test.py
    * $ python3 main_test.py
    * 순서 무관. 하나만 돌려도 상관 없음
* 테스트 항목(login_test)
    1. Check join : 가입 시 username 길이, email 형식 등 제약조건 테스트
        1. check username fail
        1. check password fail
        1. check email fail
        1. check family name fail
        1. check given name fail
        1. check join success
    1. Check login
        1. check login success
        1. check login fail
    1. Check same info : 기존 유저와 동일한 정보로 가입시 에러 발생하는 지 테스트
        1. check same username error
        1. check same email error

    * 마지막에 “Login, Join page test complete”이 출력될 경우 성공

* 테스트 항목(main_test)

    1. post test - 포스트 작성 테스트
    1. reply test - 댓글 작성 테스트
    1. reply load more test - 댓글의 더보기 버튼(load more) 테스트
    1. like test - 좋아요 테스트
    1. cancel like test - 좋아요 취소 테스트
    1. logout test - 로그아웃 테스트

    * __추가된 항목__

    1. profile test - 프로필 페이지 동작 테스트
    1. upload profile image test - 프로필 이미지 변경 테스트

    * 마지막에 “Main page test complete”이 출력될 경우 성공

# D. Redux unit test
## 1. Test result
* Main page
    * ![Alt text](http://i.imgur.com/NxCjlug.png)
* Login page
    * ![Alt text](http://i.imgur.com/BcTKK9i.png)

# E. Web service 링크
* http://swpp.devluci.com:8000/