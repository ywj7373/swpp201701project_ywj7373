# A. Bahavior list
* 로그인 (/login)
    * Back-End - 박지환
    * Front-End - 전영웅
* 로그아웃 (/logout)
    * Back-End - 박지환
* 회원가입
    * Back-End (/join) - 박지환
    * Front-End (/login) - 전영웅
    * 회원가입 Front-End는 로그인과 동일한 페이지를 사용
    * 로그인 페이지에서 Join 버튼을 누르면 화면 내 컴포넌트가 변경되면서 회원가입 화면으로 활용
* 포스트 (/post)
    * Back-End - 박지환
    * Front-End - 박희준
* 댓글달기 (/reply/<post id>)
    * Back-End - 박지환
- Back-End 테스트 코드 - 현유지
- Front-End 테스트 코드 - 전영웅

# B. Back-End
## 1. API Spec
* 포스트(/post) 추가
    * GET, POST
* 댓글(/reply/<post_id>) 추가
    * GET, POST
* 상세 양식은 https://bitbucket.org/Gamhat/swpp201701project/wiki/Server%20API

## 2. Test code
* progress/server/runtest.py
* 실행법
    * http://45.76.105.190:11178 접속(Back-End 테스트용 서버)
    * $ python3 runtest.py
* 테스트 항목
    1. Checking join
    1. Checking login
    1. Checking logout
    1. Checking writing by POST /post
    1. Checking GET by /post
    1. Checking reply by POST, GET /reply/<post id> 

    * 마지막에 “TEST SUCCESSFUL”이 출력될 경우 성공

# C. Front-End
## 1. Test code
* progress/client/login_page/login_test.py
* Selenium 활용
* 실행법
    * $ python3 login_test.py
* 테스트 항목
    1. Check join : 가입 시 username 길이, email 형식 등 제약조건 테스트
        1. check username fail
        1. check password fail
        1. check email fail
        1. check family name fail
        1. check given name fail
        1. check join success
    1. Check login
        1. check login success
        1. check login fail
    1. Check same info : 기존 유저와 동일한 정보로 가입시 에러 발생하는 지 테스트
        1. check same username error
        1. check same email error

        * 마지막에 “test complete”이 출력될 경우 성공

# D. Web service 링크
* http://45.76.105.190:3000/login/
* http://45.76.105.190:3000/