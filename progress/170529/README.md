# A. 금주 작업한 Bahavior list
* 로그아웃
    * Front-End(전영웅) - Logout 버튼 추가
* 포스트
    * Back-End(박지환) - tag, title 필드 추가
    * Front-End(전영웅) - title 추가
* 좋아요
    * Front-End(전영웅) - 좋아요 버튼 및 갯수 추가
* 댓글달기
    * Front-End(전영웅) - Load more 버튼을 통한 댓글 불러오기
* 유저 정보
    * /user/<user_id>
    * Back-End(박지환) - GET 추가
* 프로필 이미지 업로드
    * /user/image/upload
    * Back_End(박지환) - POST 추가
* 프로필
    * /profile
    * Back-End(박지환) - static file 추가
* 태그
    * /tag
    * Back-End(박지환) - GET, POST 추가
* 채팅
    * Back-End(박지환) - django-channels, django-background-tasks 추가. 추후 채팅, 크롤러 구현에 사용
* 디자인
    * CSS(전영웅) - 전체 UI 레이아웃 구성
* Jest 테스트 코드(박희준)
* Back-End 테스트 코드(현유지)
* Front-End 테스트 코드(현유지)



# B. Back-End
## 1. API Spec
* https://bitbucket.org/Gamhat/swpp201701project/wiki/Home

## 2. Test code
* progress/server/runtest.py
* 실행법
    * $ python3 runtest.py
* 테스트 항목
    * 기존 항목
    
    1. Checking join
    1. Checking login
    1. Checking logout
    1. Checking writing by POST /post
    1. Checking get post by GET /post
    1. Checking reply by POST, GET /post/<post id>/reply 
    1. Checking DELETE by /post/<post id>
    1. Checking DELETE by /post/<post id>/reply/<reply id>
    1. Checking like by POST, GET /post/<post id>/like
    1. Checking double likes is prohibited
    1. Checking DELETE /post/<post id>/like/<like id>
    
    * 추가된 항목
    
    1. Checking GET user info by /user/<user id>
    1. Checking follow by POST, GET /follow
    1. Checking double follows is prohibited
    1. Checking follow by DELETE /follow/<follow id>
    1. Checking adding tags by POST /tag
    1. Checking get tags by GET /tag

    * 마지막에 “TEST SUCCESSFUL”이 출력될 경우 성공

# C. Front-End
## 1. Test code
* progress/client/login_test.py
    * 로그인, 조인 페이지 테스트
* progress/client/main_test.py
    * 메인 페이지 테스트
* 실행법
    * $ python3 login_test.py
    * $ python3 main_test.py
    * 순서 무관. 하나만 돌려도 상관 없음
* 테스트 항목(login_test)
    1. Check join : 가입 시 username 길이, email 형식 등 제약조건 테스트
        1. check username fail
        1. check password fail
        1. check email fail
        1. check family name fail
        1. check given name fail
        1. check join success
    1. Check login
        1. check login success
        1. check login fail
    1. Check same info : 기존 유저와 동일한 정보로 가입시 에러 발생하는 지 테스트
        1. check same username error
        1. check same email error

    * 마지막에 “Login, Join page test complete”이 출력될 경우 성공

* 테스트 항목(main_test)

    1. post test - 포스트 작성 테스트
    1. reply test - 댓글 작성 테스트
    1. reply load more test - 댓글의 더보기 버튼(load more) 테스트
    1. like test - 좋아요 테스트
    1. cancel like test - 좋아요 취소 테스트
    1. logout test - 로그아웃 테스트

    * 마지막에 “Main page test complete”이 출력될 경우 성공

# D. Redux unit test
## 1. Test code
* action마다 redux state가 어떻게 변하는지 테스트
* action이 없는 기본 상태의 initialState 또한 체크
* 실행법
    * client/login_page 에서 $ npm test
* 결과
    * ![Alt text](http://i.imgur.com/n3ngYnv.png)


# E. Web service 링크
* http://swpp.devluci.com:8000/